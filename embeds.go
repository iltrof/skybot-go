package skybot

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/osu"
)

const skybotColor = 0x00A5F2

func userEmbed(users []*osu.User) *discord.Embed {
	var fields []discord.EmbedField
	for _, u := range users {
		fields = append(fields, userEmbedField(u))
	}

	thumbURL := ""
	if len(users) == 1 {
		thumbURL = users[0].AvatarURL
	}
	return &discord.Embed{
		Color:        skybotColor,
		Fields:       fields,
		ThumbnailURL: thumbURL,
	}
}

func userEmbedField(u *osu.User) discord.EmbedField {
	return discord.EmbedField{
		// \u2002 is a wider space (en space)
		Title: fmt.Sprintf("%s\u2002%s", discord.Flag(u.CountryCode), discord.Escape(u.Name)),
		Text: joinFieldItems([][]string{
			{
				format("**#%d** (#%d)", u.Rank, u.CountryRank),
				glue(format("**%f pp**", u.PP)),
			},
			{
				format("%f%", u.Accuracy),
				glue(format("%d plays", u.RankedPlays)),
				glue(format("lvl %d", int(u.Level))),
			},
			{
				discord.Link("Profile", u.ProfileURL(), ""),
				discord.Link("osu!chan", "https://syrin.me/osuchan/u/"+strconv.Itoa(u.ID), ""),
				discord.Link("osu!daily", "https://osudaily.net/profile.php?u="+u.Name, ""),
			},
		}, itemSep),
	}
}

func beatmapEmbed(beatmaps []*osu.Beatmap, ppcalcArgs *ppcalcArgs) *discord.Embed {
	var fields []discord.EmbedField
	for _, b := range beatmaps {
		fields = append(fields, beatmapEmbedField(b, ppcalcArgs))
	}

	thumbURL := ""
	if len(beatmaps) == 1 {
		thumbURL = beatmaps[0].ThumbnailURL()
	}
	return &discord.Embed{
		Color:        skybotColor,
		Fields:       fields,
		ThumbnailURL: thumbURL,
	}
}

func beatmapEmbedField(b *osu.Beatmap, ppcalcArgs *ppcalcArgs) discord.EmbedField {
	var mods string
	if b.Mods != osu.NoMod {
		mods = format("(+%s) ", b.Mods.String())
	}

	var customPP string
	if ppcalcArgs != nil {
		a := ppcalcArgs.normalized(b.MaxCombo)
		customPP = "\n" +
			formatPPCalcArgs(b.CircleCount+b.SliderCount+b.SpinnerCount, b.MaxCombo, a)
		customPP += format(" — **%f**\u00a0pp", b.PP().With(
			a.combo, a.count100, a.count50, a.missCount,
		))
	}

	return discord.EmbedField{
		Title: format("%s%s — %s [%s]", mods, b.Artist, b.Title, b.Diff),
		Text: joinFieldItems([][]string{
			{
				format("CS%f", b.CS),
				format("HP%f", b.HP),
				format("OD%f", b.OD),
				format("AR**%f**", b.AR),
			},
			{
				glue(format("**%f** BPM", b.BPM)),
				fmt.Sprintf("%d:%02d", int(b.Length.Seconds())/60, int(b.Length.Seconds())%60),
				format("%dx", b.MaxCombo),
				format("★**%f**", b.TotalStars),
				glue(format("%f pp", b.PP().SS())),
			},
			{
				discord.Link("Map", b.URL(), ""),
				discord.Link("⇓⇓⇓", b.DownloadURL(), "Download"),
				discord.Link(
					"Preview",
					fmt.Sprintf("https://bloodcat.com/osu/preview.html#%d", b.ID),
					"Bloodcat preview",
				),
				discord.Link(b.Mapper, b.MapperProfileURL(), "Creator"),
			},
		}, itemSep) + customPP,
	}
}

// \u200b is a zero-width space, to avoid
// the two spaces merging into one on a phone
const itemSep = " \u200b **·** \u200b "

func joinFieldItems(items [][]string, sep string) string {
	rows := make([]string, len(items))
	for i, r := range items {
		rows[i] = strings.Join(r, sep)
	}
	return strings.Join(rows, "\n")
}

// %d = int #,###
// %f = float64 #,###.## without trailing zeroes
// %s = string, escaped for Discord
func format(f string, a ...interface{}) string {
	b := strings.Builder{}
	b.Grow(len(f))

	arg := 0
	for i := 0; i < len(f); i++ {
		if f[i] != '%' {
			b.WriteByte(f[i])
			continue
		}
		if i == len(f)-1 {
			b.WriteByte('%')
			continue
		}

		switch f[i+1] {
		case '%':
			b.WriteByte('%')
			arg--
		case 'd':
			b.WriteString(f2f(float64(a[arg].(int))))
		case 'f':
			b.WriteString(f2f(a[arg].(float64)))
		case 's':
			b.WriteString(discord.Escape(a[arg].(string)))
		default:
			b.WriteString(fmt.Sprintf("%"+string(f[i+1]), a[arg]))
		}
		i++
		arg++
	}
	return b.String()
}

// glue makes all spaces in a string non-breakable.
func glue(s string) string {
	return strings.ReplaceAll(s, " ", "\u00a0")
}

// f2f formats a float into #,###.## with no trailing zeroes.
func f2f(f float64) string {
	s := fmt.Sprintf("%.2f", f)
	for i := strings.IndexByte(s, '.') - 3; i > 0; i -= 3 {
		s = s[:i] + "," + s[i:]
	}
	return strings.TrimRight(strings.TrimRight(s, "0"), ".")
}

func accuracy(objectCount, count100, count50, missCount int) float64 {
	count300 := objectCount - count100 - count50 - missCount
	return float64(300*count300+100*count100+50*count50) / float64(300*objectCount)
}

func formatPPCalcArgs(objectCount, maxCombo int, a ppcalcArgs) string {
	acc := accuracy(objectCount, a.count100, a.count50, a.missCount)
	if acc == 1 {
		return "SS"
	}

	result := format("%f%% ", acc*100)
	switch a.combo {
	case 0, maxCombo:
		result += "FC"
	default:
		if a.missCount != 0 {
			result += format("%dm ", a.missCount)
		}
		result += format("%dx", a.combo)
	}
	return result
}

func (a *ppcalcArgs) normalized(mapMaxCombo int) ppcalcArgs {
	r := ppcalcArgs{
		combo:     a.combo,
		count100:  a.count100,
		count50:   a.count50,
		missCount: a.missCount,
	}
	if r.combo == 0 {
		r.combo = mapMaxCombo - r.missCount
	}

	needsComboBreaks := mapMaxCombo / (r.combo + 1)
	needsComboBreaks -= r.missCount + r.count100 + r.count50
	if needsComboBreaks > 0 {
		r.count100 += needsComboBreaks
	}

	return r
}
