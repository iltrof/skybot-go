package skybot

import (
	"testing"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/stretchr/testify/assert"
)

func Test_newSettingArgs(t *testing.T) {
	tests := []struct {
		name   string
		argStr string
		msg    *discord.Message

		canManageChannel bool
		canManageServer  bool

		want         *settingArgs
		wantFriendly string
	}{
		{"empty", "", &discord.Message{}, true, true, nil, errSettingArgsMissingSubcommand},
		{"just command", "foo", &discord.Message{AuthorID: "42"}, true, true, &settingArgs{
			command: "foo", level: db.LevelUser, id: "42", argStr: "",
		}, ""},
		{"with args", "foo abc def", &discord.Message{AuthorID: "42"}, true, true, &settingArgs{
			command: "foo", level: db.LevelUser, id: "42", argStr: "abc def",
		}, ""},
		{"just level", "channel", &discord.Message{}, true, true, nil, errSettingArgsMissingSubcommand},
		{"level and command", "s prefix", &discord.Message{ServerID: "77"}, true, true, &settingArgs{
			command: "prefix", level: db.LevelServer, id: "77", argStr: "",
		}, ""},
		{"everything", "chan foo bar", &discord.Message{ChannelID: "100"}, true, true, &settingArgs{
			command: "foo", level: db.LevelChannel, id: "100", argStr: "bar",
		}, ""},
		{"level is case-insensitive", "ChAn Foo BAR", &discord.Message{ChannelID: "100"}, true, true, &settingArgs{
			command: "Foo", level: db.LevelChannel, id: "100", argStr: "BAR",
		}, ""},

		{"no channel perm", "chan foo", &discord.Message{ChannelID: "100"}, false, true, nil,
			errCannotManageChannel},
		{"no server perm", "server foo", &discord.Message{ChannelID: "100"}, true, false, nil,
			errCannotManageServer},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.msg.SetAuthorManageChannelPerm(tt.canManageChannel)
			tt.msg.SetAuthorManageServerPerm(tt.canManageServer)

			got, err := newSettingArgs(&Bot{}, tt.argStr, tt.msg)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
		})
	}
}
