package friendlyerr

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/xerrors"
)

func TestWrapf(t *testing.T) {
	err := Wrapf(nil, "")
	assert.Nil(t, err)

	err = Wrapf(fmt.Errorf("bad thing 1"), "")
	assert.Error(t, err, "bad thing 1")

	err = Wrapf(xerrors.New("bad thing 2"), "")
	assert.Error(t, err, "bad thing 2")
	assert.Contains(t, fmt.Sprintf("%+v", err),
		"github.com/iltrof/skybot/friendlyerr.TestWrap",
		"should preserve xerrors stack trace")
}

func TestDescription(t *testing.T) {
	assert.Empty(t, Description(nil))

	desc := Description(xerrors.New("bad thing"))
	assert.Empty(t, desc)

	err := Wrapf(xerrors.New("bad thing"), "foo")
	desc = Description(err)
	assert.Equal(t, "foo", desc)

	err = xerrors.Errorf("wrapped: %w", err)
	desc = Description(err)
	assert.Equal(t, "foo", desc, "should unwrap")
}
