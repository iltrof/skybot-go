package friendlyerr

import (
	"fmt"

	"golang.org/x/xerrors"
)

// Wrapf wraps an error with its user-friendly description.
// If the error is nil, New also returns nil.
func Wrapf(err error, format string, a ...interface{}) error {
	if err == nil {
		return nil
	}
	return &wrapper{
		friendly: fmt.Sprintf(format, a...),
		error:    err,
	}
}

// Description returns the user-friendly description
// of the error, as long as it has been created with
// friendlyerr.Wrapf. Otherwise returns empty string.
func Description(err error) string {
	var e *wrapper
	ok := xerrors.As(err, &e)
	if !ok {
		return ""
	}
	return e.friendly
}

type wrapper struct {
	friendly string
	error
}

func (w *wrapper) Error() string {
	return w.error.Error()
}

func (w *wrapper) Format(f fmt.State, c rune) {
	xerrors.FormatError(w, f, c)
}

func (w *wrapper) FormatError(p xerrors.Printer) error {
	if e, ok := w.error.(xerrors.Formatter); ok {
		_ = e.FormatError(p)
		return nil
	}

	p.Print(w.error)
	return nil
}

func (w *wrapper) Unwrap() error {
	return w.error
}
