package skybot

import (
	"testing"

	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/osu"
	"github.com/stretchr/testify/assert"
)

func Test_expandMods(t *testing.T) {
	tests := []struct {
		s            string
		maxCombs     int
		want         []osu.Mods
		wantFriendly string
	}{
		{"", 1, []osu.Mods{osu.NoMod}, ""},
		{"hd", 1, []osu.Mods{osu.Hidden}, ""},
		{"hd?dt", 2, []osu.Mods{
			osu.DoubleTime,
			osu.Hidden | osu.DoubleTime,
		}, ""},

		{"dt", 1, []osu.Mods{osu.DoubleTime}, ""},
		{"dt", 2, []osu.Mods{
			osu.DoubleTime,
			osu.Nightcore | osu.DoubleTime,
		}, ""},
		{"nc", 2, []osu.Mods{
			osu.DoubleTime,
			osu.Nightcore | osu.DoubleTime,
		}, ""},

		{"sd", 1, []osu.Mods{osu.SuddenDeath}, ""},
		{"sd", 2, []osu.Mods{
			osu.SuddenDeath,
			osu.Perfect | osu.SuddenDeath,
		}, ""},
		{"pf", 2, []osu.Mods{
			osu.SuddenDeath,
			osu.Perfect | osu.SuddenDeath,
		}, ""},

		{"hd?nc?", 5, []osu.Mods{
			osu.NoMod,
			osu.Hidden,
			osu.Nightcore | osu.DoubleTime,
			osu.Hidden | osu.Nightcore | osu.DoubleTime,
		}, ""},
		{"hd?dt?", 8, []osu.Mods{
			osu.NoMod,
			osu.Hidden,
			osu.DoubleTime,
			osu.Nightcore | osu.DoubleTime,
			osu.Hidden | osu.DoubleTime,
			osu.Hidden | osu.Nightcore | osu.DoubleTime,
		}, ""},

		{"sddt", 8, []osu.Mods{
			osu.SuddenDeath | osu.DoubleTime,
			osu.Perfect | osu.SuddenDeath | osu.DoubleTime,
			osu.SuddenDeath | osu.Nightcore | osu.DoubleTime,
			osu.Perfect | osu.SuddenDeath | osu.Nightcore | osu.DoubleTime,
		}, ""},
		{"ncpf", 8, []osu.Mods{
			osu.SuddenDeath | osu.DoubleTime,
			osu.Perfect | osu.SuddenDeath | osu.DoubleTime,
			osu.SuddenDeath | osu.Nightcore | osu.DoubleTime,
			osu.Perfect | osu.SuddenDeath | osu.Nightcore | osu.DoubleTime,
		}, ""},

		{"hd?dt", 1, nil, errTooManyMods(1)},
		{"hd?hr?dt?", 7, nil, errTooManyMods(7)},
		{"uh", 2, nil, errInvalidMods},
		{"hduhdt", 2, nil, errInvalidMods},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			m, err := expandMods(tt.s, tt.maxCombs)
			assert.ElementsMatch(t, tt.want, m)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
		})
	}
}
