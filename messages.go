package skybot

import (
	"fmt"
	"strings"

	"github.com/iltrof/skybot/discord"
)

func errNoUserWithName(name string) string {
	return fmt.Sprintf(`There is no user called "%s". Did you make a mistake?`, discord.Escape(name))
}

func errNoUserWithID(id string) string {
	return fmt.Sprintf(`There is no user with ID %s. Did you make a mistake?`, id)
}

func errArgsNeedUsersRange(min, max, given int) string {
	s := fmt.Sprintf("This command needs %s, but ", pluralizeRange(min, max, "user", "users"))
	if given == 0 {
		return s + "you have not given me any."
	}
	return s + fmt.Sprintf("you have given me %d.", given)
}

func errArgsNeedBeatmapsRange(min, max, given int) string {
	s := fmt.Sprintf("This command needs %s, but ", pluralizeRange(min, max, "beatmap", "beatmaps"))
	if given == 0 {
		return s + "you have not given me any."
	}
	return s + fmt.Sprintf("you have given me %d.", given)
}

func errUnknownOsuIDAuthor(cmdPrefix string) string {
	return fmt.Sprintf("I don't know your osu! account yet! This command was missing a user, and I would "+
		"normally take your osu! user, but you need to tell me who you are first. "+
		"Please use %s to do so.", stylizeCommand(cmdPrefix, "skybot bind", "your-osu-name"))
}

func errUnknownOsuIDOther(otherID, cmdPrefix string) string {
	return fmt.Sprintf("I don't know who %s is. They need to tell me their osu! name using "+
		"%s first.", discord.MentionString(otherID), stylizeCommand(cmdPrefix, "skybot bind", "their-osu-name"))
}

func errDeletedOsuUserAuthor(cmdPrefix string) string {
	return fmt.Sprintf("It seems that your osu! account has been deleted/restricted. "+
		"If this doesn't sound right, try specifying your account anew "+
		"using %s.", stylizeCommand(cmdPrefix, "skybot bind", "your-osu-name"))
}

func errDeletedOsuUserOther(otherID, cmdPrefix string) string {
	return fmt.Sprintf("It seems that %s's osu! account has been deleted/restricted. "+
		"If this doesn't sound right, they can try specifying their account anew "+
		"using %s.", discord.MentionString(otherID), stylizeCommand(cmdPrefix, "skybot bind", "their-osu-name"))
}

func errUnknownSetting(setting string) string {
	return fmt.Sprintf(`I don't know what "%s" should mean :thinking:`, setting)
}

func errBindOsuIDMissingName(cmdPrefix string) string {
	return fmt.Sprintf("What's your osu! username? (Usage: %s)",
		stylizeCommand(cmdPrefix, "skybot bind", "your-osu-name"))
}

func successBindOsuID(name string, id int) string {
	return fmt.Sprintf("Done! You are now bound to %s (ID %d).", discord.Escape(name), id)
}

func errAddAliases(cmdPrefix string) string {
	return fmt.Sprintf("Not sure what you want me to do... (Usage: %s)",
		stylizeCommand(cmdPrefix, "skybot alias", "short-name[, short-name2, ...] for full-name"))
}

func successAddAliases(fullName string, id int, shortNames ...string) string {
	escaped := make([]string, len(shortNames))
	for i, n := range shortNames {
		escaped[i] = discord.Escape(n)
	}
	return fmt.Sprintf("Done! %s now %s to %s (ID %d).", list(escaped...),
		pluralize(len(shortNames), "aliases", "alias"), discord.Escape(fullName), id)
}

func errTooManyMods(max int) string {
	return fmt.Sprintf("No more than %d %s, please.", max,
		pluralize(max, "mod combination", "mod combinations"))
}

func errUnknownToken(s string) string {
	return fmt.Sprintf(`"%s" doesn't look correct to me. Did you make a mistake?`, s)
}

const (
	errDefault        = "Something unexpected went wrong... You can try again, but who knows if it'll work."
	errUnknownCommand = "I'm not sure what you want me to do :thinking:"

	errSettingArgsMissingSubcommand = "What do you want me to do?"
	errManageChannelCheckFailed     = "I wasn't able to verify that you have " +
		"the permission to manage this channel. Try again later?"
	errManageServerCheckFailed = "I wasn't able to verify that you have " +
		"the permission to manage this server. Try again later?"
	errCannotManageChannel = "You don't have the permissions to manage this channel."
	errCannotManageServer  = "You don't have the permissions to manage this server."

	errSettingUserOnly = "This command can only be run at the user level."

	errNoBeatmap         = "There's no beatmap in that link. Did you make a mistake?"
	errNoLastUsedBeatmap = "I'm not sure what beatmap you want me to use..."

	errInvalidMods = "These mods don't look valid to me."
)

func stylizeCommand(cmdPrefix, name string, args ...string) string {
	return fmt.Sprintf("**%s%s %s**", discord.Escape(cmdPrefix),
		discord.Escape(name), discord.Escape(strings.Join(args, " ")))
}

func pluralizeRange(min, max int, singular, plural string) string {
	if min == 0 && max == 0 {
		return "no " + plural
	}
	if min == 1 && max == 1 {
		return "one " + singular
	}
	if min == max {
		return fmt.Sprintf("%d %s", min, plural)
	}
	return fmt.Sprintf("%d–%d %s", min, max, plural)
}

func pluralize(count int, singular, plural string) string {
	switch count {
	case 1:
		return singular
	default:
		return plural
	}
}

func list(items ...string) string {
	switch len(items) {
	case 0:
		return ""
	case 1:
		return items[0]
	default:
		return strings.Join(items[:len(items)-1], ", ") + " and " + items[len(items)-1]
	}
}
