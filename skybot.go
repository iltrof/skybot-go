//go:generate mockery -name=OsuAPI -case=underscore
//go:generate mockery -name=DB -case=underscore

package skybot

import (
	"log"
	"strings"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/osu"
)

// Bot is the heart of this application. It is an osu! bot for Discord
// which allows osu! players to share/look up their and others' scores.
type Bot struct {
	Discord  *discord.Client
	Osu      OsuAPI
	Settings DB
}

// Run runs the bot.
func (b *Bot) Run() {
	b.Discord.AddHandler(b.mainHandler(map[string]commandHandler{
		"u": showUser, "user": showUser,
		"m": showBeatmap, "map": showBeatmap,

		"skybot": settingsHandler(map[string]settingHandler{
			"bind":  bindOsuID,
			"alias": addAliases,
		}),
	}))
}

// Stop stops the bot.
func (b *Bot) Stop() {
	b.Discord.Close()
}

const cmdPrefix = "`"

func (b *Bot) mainHandler(handlers map[string]commandHandler) func(*discord.Replier, *discord.Message) {
	return func(r *discord.Replier, m *discord.Message) {
		if !strings.HasPrefix(m.Text, cmdPrefix) {
			return
		}

		msg := strings.TrimSpace(strings.TrimPrefix(m.Text, cmdPrefix))
		if msg == "" {
			return
		}
		name, args := splitCommandName(msg)

		handler := handlers[strings.ToLower(name)]
		if handler == nil {
			err := r.Replyf(errUnknownCommand)
			if err != nil {
				log.Println(err)
			}
			return
		}

		defer func() {
			if err := recover(); err != nil {
				log.Println(err)
			}
			// A command handler should always reply at least something.
			// If not, send a default reply (Replyf is sync.Once'd).
			err := r.Replyf(errDefault)
			if err != nil {
				log.Println(err)
			}
		}()

		err := handler(b, r, m, args)
		if err != nil {
			log.Println(err)
			desc := friendlyerr.Description(err)
			if desc == "" {
				desc = errDefault
			}
			err := r.Replyf(desc)
			if err != nil {
				log.Println(err)
			}
		}
	}
}

// OsuAPI wraps functions used to make calls to the osu! API.
type OsuAPI interface {
	GetUserByID(string) (*osu.User, error)
	GetUserByName(string) (*osu.User, error)
	GetBeatmap(string, osu.Mods) (*osu.Beatmap, error)
	GetBeatmapSet(string, osu.Mods) ([]*osu.Beatmap, error)
}

// DB wraps functions for operating a database.
type DB interface {
	Set(level db.HierLevel, id, key, value string) error
	Get(level db.HierLevel, id, key string) string
	GetAny(servID, chanID, userID, key string) string
}
