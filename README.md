# skyBot-go

What intended to be a rewrite of [skybot-js](https://gitlab.com/iltrof/skybot-js), a Discord bot based around osu!

Has a few features and more polish than the js version, but definitely isn't complete.

# Running

Specify the environment variables:

- `OSUAPI_TOKEN` - your osu! API token
- `DISCORD_TOKEN` - your Discord bot token

Then run `go run ./cmd/skybot`.

# Features

All commands start with the backtick (`` ` ``).

## Binding your Discord ID to osu! username

`` `skybot bind your-osu-name`` associates your discord ID with your osu username. Doing that allows you to use a command on yourself without specifying your name every time. (E.g. writing just `` `u`` instead of `` `u your-name`` to see your stats.)

## Adding aliases for people's usernames

Some players' names can be long and tedious to type, so it's possible to add a short name for them. The syntax is:

`` `skybot alias short-name for full-name``.

This command can be further differentiated by where you want the short name to work:

- `` `skybot user alias ...`` only creates an alias that works only for the user who wrote the command
- `` `skybot channel alias ...`` creates an alias that works inside the channel that command was written in
- `` `skybot server alias ...`` creates an alias that works inside the entire server

By default the alias is created for the entire server.

## User info

`` `u username` ``/`` `user username`` displays information about a user.

![](screenshots/user_command.png)

If you have bound your Discord ID to your username, omitting the name will show your own stats.

Instead of giving the command a name, you can also use a link to the user.

## Beatmap info

`` `link-to-beatmap +mods`` displays information about a beatmap.

The ability to specify mods is the big improvement in this version. The bot will properly calculate the stats for the given mods.

You can specify them with a plus followed by a combination of: `ez`, `hd`, `hr`, `ht`, `dt`, `nc`, `fl`, `nf`, `so`, `sd`, `pf`, `td`. For example, `+hddt` indicates Hidden + DoubleTime.

![](screenshots/beatmap_command.png)
