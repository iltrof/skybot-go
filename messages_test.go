package skybot

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_stylizeCommand(t *testing.T) {
	assert.Equal(t, "**\\`bindme foo bar\\-bar**", stylizeCommand("`", "bindme", "foo", "bar-bar"))
}

func Test_pluralizeRange(t *testing.T) {
	type args struct {
		min      int
		max      int
		singular string
		plural   string
	}
	tests := []struct {
		args args
		want string
	}{
		{args{0, 0, "x", "xs"}, "no xs"},
		{args{1, 1, "x", "xs"}, "one x"},
		{args{3, 3, "x", "xs"}, "3 xs"},
		{args{0, 1, "x", "xs"}, "0–1 xs"}, // en dash, not a normal one
		{args{0, 2, "x", "xs"}, "0–2 xs"},
		{args{1, 2, "x", "xs"}, "1–2 xs"},
		{args{3, 6, "x", "xs"}, "3–6 xs"},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%d-%d", tt.args.min, tt.args.max), func(t *testing.T) {
			if got := pluralizeRange(tt.args.min, tt.args.max, tt.args.singular, tt.args.plural); got != tt.want {
				t.Errorf("pluralizeRange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_list(t *testing.T) {
	tests := []struct {
		name  string
		items []string
		want  string
	}{
		{"none", []string{}, ""},
		{"one", []string{"apple"}, "apple"},
		{"two", []string{"apple", "pear"}, "apple and pear"},
		{"more", []string{"apple", "pear", "orange", "banana"}, "apple, pear, orange and banana"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, list(tt.items...))
		})
	}
}
