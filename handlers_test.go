package skybot

import (
	"testing"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/discord/disctest"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/mocks"
	"github.com/iltrof/skybot/osu"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"golang.org/x/xerrors"
)

func (s *handlerSuite) Test_showUser() {
	s.osuAPI.On("GetUserByName", "iltrof").Return(&osu.User{}, (error)(nil)).Once()
	err := showUser(s.bot, s.rr.Replier(), &discord.Message{}, "iltrof")

	s.NoError(err)
	s.Equal("", s.rr.Text)
	s.Equal(userEmbed([]*osu.User{{}}), s.rr.Embed)
}

func (s *handlerSuite) Test_showBeatmap() {
	s.osuAPI.On("GetBeatmap", "75", osu.NoMod).Return(&sampleBeatmap, (error)(nil)).Once()
	s.settings.On("Set", db.LevelChannel, "123", "lastMap", "75").Return((error)(nil)).Once()
	err := showBeatmap(s.bot, s.rr.Replier(), &discord.Message{ChannelID: "123"}, "https://osu.ppy.sh/b/75")

	s.NoError(err)
	s.Equal("", s.rr.Text)
	s.Equal(beatmapEmbed([]*osu.Beatmap{&sampleBeatmap}, nil), s.rr.Embed)
}

func (s *handlerSuite) Test_showBeatmap_ppArgs() {
	s.osuAPI.On("GetBeatmap", "75", osu.NoMod).Return(&sampleBeatmap, (error)(nil))
	s.settings.On("Set", db.LevelChannel, "123", "lastMap", "75").Return((error)(nil))
	err := showBeatmap(s.bot, s.rr.Replier(), &discord.Message{ChannelID: "123"},
		"https://osu.ppy.sh/b/75 42x 5x100 3x50 2m")

	s.NoError(err)
	s.Equal("", s.rr.Text)
	s.Equal(beatmapEmbed([]*osu.Beatmap{&sampleBeatmap}, &ppcalcArgs{
		count100: 5, count50: 3, missCount: 2, combo: 42,
	}), s.rr.Embed)
}

func (s *handlerSuite) Test_bindOsuID() {
	s.osuAPI.On("GetUserByName", "iltrof").Return(&osu.User{Name: "iltrof", ID: 123}, (error)(nil)).Once()
	s.settings.On("Set", db.LevelUser, "42", "osuID", "123").Return((error)(nil)).Once()

	err := bindOsuID(s.bot, s.rr.Replier(), db.LevelUser, "42", "iltrof")
	s.NoError(err)
	s.Equal(successBindOsuID("iltrof", 123), s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_bindOsuID_simpleErrors() {
	err := bindOsuID(s.bot, s.rr.Replier(), db.LevelUser, "42", "")
	s.Equal(errBindOsuIDMissingName(cmdPrefix), friendlyerr.Description(err))

	err = bindOsuID(s.bot, s.rr.Replier(), db.LevelChannel, "42", "iltrof")
	s.Equal(errSettingUserOnly, friendlyerr.Description(err))

	err = bindOsuID(s.bot, s.rr.Replier(), db.LevelServer, "42", "iltrof")
	s.Equal(errSettingUserOnly, friendlyerr.Description(err))

	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_bindOsuID_osuapiDown() {
	s.osuAPI.On("GetUserByName", "iltrof").Return((*osu.User)(nil),
		friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")).Once()

	err := bindOsuID(s.bot, s.rr.Replier(), db.LevelUser, "42", "iltrof")
	s.Equal("Bad thing happened.", friendlyerr.Description(err))
	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_bindOsuID_databaseError() {
	s.osuAPI.On("GetUserByName", "iltrof").Return(&osu.User{Name: "iltrof", ID: 123}, (error)(nil)).Once()
	s.settings.On("Set", db.LevelUser, "42", "osuID", "123").Return(
		friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")).Once()

	err := bindOsuID(s.bot, s.rr.Replier(), db.LevelUser, "42", "iltrof")
	s.Equal("Bad thing happened.", friendlyerr.Description(err))
	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_addAliases() {
	s.osuAPI.On("GetUserByName", "nathan on osu").Return(&osu.User{Name: "nathan on osu", ID: 123}, (error)(nil)).Once()
	s.settings.On("Set", db.LevelUser, "42", "alias:shige", "123").Return((error)(nil)).Once()

	err := addAliases(s.bot, s.rr.Replier(), db.LevelUser, "42", "shige for nathan on osu")
	s.NoError(err)
	s.Equal(successAddAliases("nathan on osu", 123, "shige"), s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_addAliases2() {
	s.osuAPI.On("GetUserByName", "wubwoofwolf").Return(&osu.User{Name: "WubWoofWolf", ID: 123}, (error)(nil)).Once()
	s.settings.On("Set", db.LevelServer, "42", "alias:www", "123").Return((error)(nil)).Once()
	s.settings.On("Set", db.LevelServer, "42", "alias:whitewolf", "123").Return((error)(nil)).Once()
	s.settings.On("Set", db.LevelServer, "42", "alias:foo for bar", "123").Return((error)(nil)).Once()

	err := addAliases(s.bot, s.rr.Replier(), db.LevelServer, "42",
		"www, foo for bar , , WhiteWolf For wubwoofwolf")
	s.NoError(err)
	s.Equal(successAddAliases("WubWoofWolf", 123, "www", "foo for bar", "whitewolf"), s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_addAliases_badArgs() {
	err := addAliases(s.bot, s.rr.Replier(), db.LevelUser, "42", "")
	s.Equal(errAddAliases(cmdPrefix), friendlyerr.Description(err))

	err = addAliases(s.bot, s.rr.Replier(), db.LevelUser, "42", "www wubwoofwolf")
	s.Equal(errAddAliases(cmdPrefix), friendlyerr.Description(err))

	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_addAliases_osuapiDown() {
	s.osuAPI.On("GetUserByName", "nathan on osu").Return((*osu.User)(nil),
		friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")).Once()

	err := addAliases(s.bot, s.rr.Replier(), db.LevelUser, "42", "shige for nathan on osu")
	s.Equal("Bad thing happened.", friendlyerr.Description(err))
	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

func (s *handlerSuite) Test_addAliases_databaseError() {
	s.osuAPI.On("GetUserByName", "nathan on osu").Return(&osu.User{Name: "nathan on osu", ID: 123}, (error)(nil)).Once()
	s.settings.On("Set", db.LevelUser, "42", "alias:shige", "123").Return(
		friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")).Once()

	err := addAliases(s.bot, s.rr.Replier(), db.LevelUser, "42", "shige for nathan on osu")
	s.Equal("Bad thing happened.", friendlyerr.Description(err))
	s.Empty(s.rr.Text)
	s.Nil(s.rr.Embed)
}

type handlerSuite struct {
	suite.Suite
	osuAPI   *mocks.OsuAPI
	settings *mocks.DB
	bot      *Bot
	rr       disctest.ReplyRecorder
}

func (s *handlerSuite) SetupTest() {
	s.osuAPI = &mocks.OsuAPI{}
	s.settings = &mocks.DB{}
	s.settings.On("GetAny", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("").Maybe()
	s.bot = &Bot{Osu: s.osuAPI, Settings: s.settings}
	s.rr = disctest.ReplyRecorder{}
}

func (s *handlerSuite) TearDownTest() {
	s.osuAPI.AssertExpectations(s.T())
	s.settings.AssertExpectations(s.T())
}

func TestHandlers(t *testing.T) {
	suite.Run(t, &handlerSuite{})
}

func Test_settingsHandler(t *testing.T) {
	rr := disctest.ReplyRecorder{}
	count := 0
	h := settingsHandler(map[string]settingHandler{
		"foo": func(b *Bot, r *discord.Replier, level db.HierLevel, id string, argStr string) error {
			assert.Equal(t, db.LevelUser, level)
			assert.Equal(t, "42", id)
			assert.Equal(t, "bar", argStr)
			count++
			return nil
		},
	})

	tests := []struct {
		name   string
		argStr string

		wantFriendly string
		wantCount    int
	}{
		{"no args", "", errSettingArgsMissingSubcommand, 0},
		{"unknown setting", "user bar", errUnknownSetting("bar"), 0},
		{"normal", "user foo bar", "", 1},
		{"case-insensitive", "user FoO bar", "", 1},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			count = 0
			err := h(&Bot{}, rr.Replier(), &discord.Message{AuthorID: "42"}, tt.argStr)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
			assert.Equal(t, tt.wantCount, count)
		})
	}

	assert.Empty(t, rr.Text)
	assert.Nil(t, rr.Embed)
}
