package skybot

import (
	"testing"

	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/discord/disctest"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/stretchr/testify/assert"
	"golang.org/x/xerrors"
)

func Test_mainHandler(t *testing.T) {
	bot := Bot{}
	main := bot.mainHandler(map[string]commandHandler{
		"foo": testHandler("foo"),
		"bar": testHandler("bar"),
	})

	tests := []struct {
		name string
		msg  string
		want string
	}{
		{"empty", "", ""},
		{"not a command", "foo", ""},
		{"empty command", cmdPrefix, ""},
		{"unknown", cmdPrefix + "unknown", errUnknownCommand},
		{"normal", cmdPrefix + "foo 123", "foo"},
		{"mixed case", cmdPrefix + "FoO", "foo"},
		{"with space", cmdPrefix + " foo", "foo"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := disctest.ReplyRecorder{}
			main(rr.Replier(), &discord.Message{Text: tt.msg})
			assert.Nil(t, rr.Embed)
			assert.Equal(t, tt.want, rr.Text)
		})
	}
}

func Test_mainHandler_errors(t *testing.T) {
	tests := []struct {
		name string
		hand commandHandler
		want string
	}{
		{"panic", func(*Bot, *discord.Replier, *discord.Message, string) error {
			panic("handler 1")
		}, errDefault},
		{"raw error", func(*Bot, *discord.Replier, *discord.Message, string) error {
			return xerrors.New("handler 2")
		}, errDefault},
		{"friendly error", func(*Bot, *discord.Replier, *discord.Message, string) error {
			return friendlyerr.Wrapf(xerrors.New("handler 3"), "friendly")
		}, "friendly"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			bot := Bot{}
			main := bot.mainHandler(map[string]commandHandler{"hand": tt.hand})
			rr := disctest.ReplyRecorder{}
			main(rr.Replier(), &discord.Message{Text: cmdPrefix + "hand"})
			assert.Equal(t, tt.want, rr.Text)
		})
	}
}

func testHandler(reply string) commandHandler {
	return func(_ *Bot, r *discord.Replier, _ *discord.Message, _ string) error {
		_ = r.Replyf(reply)
		return nil
	}
}
