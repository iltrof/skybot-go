package osu

import (
	"fmt"
	"net/url"
	"time"

	"golang.org/x/xerrors"
)

// User is an osu! user.
type User struct {
	ID          int
	Name        string
	CountryCode string
	AvatarURL   string

	PP          float64
	Rank        int
	CountryRank int

	Accuracy       float64
	RankedPlays    int
	RankedPlayTime time.Duration
	Level          float64

	CountSS  int
	CountSSH int
	CountS   int
	CountSH  int
	CountA   int
}

// GetUserByID returns the user with the given ID.
func (c *Client) GetUserByID(id string) (*User, error) {
	return c.getUser(fmt.Sprintf("%s/api/get_user?k=%s&u=%s&type=id",
		c.apiBase, c.apiToken, id))
}

// GetUserByName returns the user with the given name.
func (c *Client) GetUserByName(name string) (*User, error) {
	return c.getUser(fmt.Sprintf("%s/api/get_user?k=%s&u=%s&type=string",
		c.apiBase, c.apiToken, url.QueryEscape(name)))
}

func (c *Client) getUser(url string) (*User, error) {
	var u []apiUser
	err := c.getJSON(&u, url)
	if err != nil {
		return nil, xerrors.Errorf("get %s: %w", url, err)
	}
	if len(u) == 0 {
		return nil, nil
	}

	return u[0].user(), nil
}

type apiUser struct {
	UserID             int        `json:"user_id,omitempty,string"`
	Username           string     `json:"username,omitempty"`
	JoinDate           apiDate    `json:"join_date,omitempty"`
	Count300           int        `json:"count300,omitempty,string"`
	Count100           int        `json:"count100,omitempty,string"`
	Count50            int        `json:"count50,omitempty,string"`
	Playcount          int        `json:"playcount,omitempty,string"`
	RankedScore        int64      `json:"ranked_score,omitempty,string"`
	TotalScore         int64      `json:"total_score,omitempty,string"`
	PPRank             int        `json:"pp_rank,omitempty,string"`
	Level              float64    `json:"level,omitempty,string"`
	PPRaw              float64    `json:"pp_raw,omitempty,string"`
	Accuracy           float64    `json:"accuracy,omitempty,string"`
	CountRankSS        int        `json:"count_rank_ss,omitempty,string"`
	CountRankSSH       int        `json:"count_rank_ssh,omitempty,string"`
	CountRankS         int        `json:"count_rank_s,omitempty,string"`
	CountRankSH        int        `json:"count_rank_sh,omitempty,string"`
	CountRankA         int        `json:"count_rank_a,omitempty,string"`
	Country            string     `json:"country,omitempty"`
	TotalSecondsPlayed apiSeconds `json:"total_seconds_played,omitempty"`
	PPCountryRank      int        `json:"pp_country_rank,omitempty,string"`
	Events             []struct {
		DisplayHTML  string  `json:"display_html,omitempty"`
		BeatmapID    int     `json:"beatmap_id,omitempty,string"`
		BeatmapsetID int     `json:"beatmapset_id,omitempty,string"`
		Date         apiDate `json:"date,omitempty"`
		Epicfactor   int     `json:"epicfactor,omitempty,string"`
	} `json:"events,omitempty"`
}

func (u *apiUser) user() *User {
	return &User{
		ID:          u.UserID,
		Name:        u.Username,
		CountryCode: u.Country,
		AvatarURL:   fmt.Sprintf("http://a.ppy.sh/%d", u.UserID),

		PP:          u.PPRaw,
		Rank:        u.PPRank,
		CountryRank: u.PPCountryRank,

		Accuracy:       u.Accuracy,
		RankedPlays:    u.Playcount,
		RankedPlayTime: u.TotalSecondsPlayed.Duration,
		Level:          u.Level,

		CountSS:  u.CountRankSS,
		CountSSH: u.CountRankSSH,
		CountS:   u.CountRankS,
		CountSH:  u.CountRankSH,
		CountA:   u.CountRankA,
	}
}

// ProfileURL returns a URL to the user's profile.
func (u *User) ProfileURL() string {
	return fmt.Sprintf("https://osu.ppy.sh/users/%d", u.ID)
}
