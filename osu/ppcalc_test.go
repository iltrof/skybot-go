package osu

import (
	"encoding/csv"
	"math"
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_ppCalc(t *testing.T) {
	recs := loadPPTests(t)
	for _, rec := range recs {
		test := newPPTest(rec)
		c := NewPPCalc(test.beatmap())
		pp := c.calcPP(test.scoreCombo, test.count100, test.count50, test.missCount)
		assert.InDelta(t, test.pp, pp, 0.49)
	}
}

func Benchmark_ppCalc(b *testing.B) {
	b.StopTimer()
	recs := loadPPTests(b)

	b.StartTimer()
	var meanDiff, maxDiff float64
	for i := 0; i < b.N; i++ {
		meanDiff = 0.0
		maxDiff = 0.0
		for _, rec := range recs {
			test := newPPTest(rec)
			c := NewPPCalc(test.beatmap())
			pp := c.calcPP(test.scoreCombo, test.count100, test.count50, test.missCount)

			diff := math.Abs(float64(pp) - test.pp)
			meanDiff += diff
			if diff > maxDiff {
				maxDiff = diff
			}
		}
	}

	meanDiff /= float64(len(recs))
	b.Logf("Mean diff.: %f, Max diff.: %f", meanDiff, maxDiff)
}

func loadPPTests(t require.TestingT) [][]string {
	testFile, err := os.Open("testdata/pp_test.csv")
	require.NoError(t, err)

	r := csv.NewReader(testFile)
	r.Comment = '#'
	recs, err := r.ReadAll()
	require.NoError(t, err)
	return recs
}

type ppTest struct {
	circleCount  int
	sliderCount  int
	spinnerCount int
	mapMaxCombo  int
	baseAR       float64
	baseOD       float64
	aimStars     float64
	speedStars   float64
	mods         Mods
	scoreCombo   int
	count100     int
	count50      int
	missCount    int
	pp           float64
}

func newPPTest(csv []string) ppTest {
	return ppTest{
		circleCount:  mustAtoi(csv[1]),
		sliderCount:  mustAtoi(csv[2]),
		spinnerCount: mustAtoi(csv[3]),
		mapMaxCombo:  mustAtoi(csv[4]),
		baseAR:       mustAtof(csv[5]),
		baseOD:       mustAtof(csv[6]),
		aimStars:     mustAtof(csv[7]),
		speedStars:   mustAtof(csv[8]),
		mods:         Mods(mustAtoi(csv[9])),
		scoreCombo:   mustAtoi(csv[10]),
		count100:     mustAtoi(csv[11]),
		count50:      mustAtoi(csv[12]),
		missCount:    mustAtoi(csv[13]),
		pp:           mustAtof(csv[14]),
	}
}

func (t ppTest) beatmap() *Beatmap {
	return &Beatmap{
		CircleCount:  t.circleCount,
		SliderCount:  t.sliderCount,
		SpinnerCount: t.spinnerCount,
		MaxCombo:     t.mapMaxCombo,
		baseAR:       t.baseAR,
		baseOD:       t.baseOD,
		AimStars:     t.aimStars,
		SpeedStars:   t.speedStars,
		Mods:         t.mods,
	}
}

func mustAtoi(s string) int {
	v, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return v
}

func mustAtof(s string) float64 {
	v, err := strconv.ParseFloat(s, 64)
	if err != nil {
		panic(err)
	}
	return v
}
