package osu

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_arMSConversion(t *testing.T) {
	tests := []struct {
		ar float64
		ms float64
	}{
		{0, 1800}, {2, 1560},
		{5, 1200}, {6, 1050},
		{8, 750}, {11, 300},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.ar), func(t *testing.T) {
			assert.Equal(t, tt.ms, arToMS(tt.ar))
			assert.Equal(t, tt.ar, msToAR(tt.ms))
		})
	}
}

func Test_odMSConversion(t *testing.T) {
	tests := []struct {
		od float64
		ms float64
	}{{0, 79.5}, {5, 49.5}, {10, 19.5}}

	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.od), func(t *testing.T) {
			assert.Equal(t, tt.ms, odToMS(tt.od))
			assert.Equal(t, tt.od, msToOD(tt.ms))
		})
	}
}

func TestMods_ApplyToStats(t *testing.T) {
	type stats struct {
		cs float64
		hp float64
		od float64
		ar float64
	}
	tests := []struct {
		m    Mods
		s    stats
		want stats
	}{
		{NoMod, stats{1, 2, 3, 4}, stats{1, 2, 3, 4}},
		{HalfTime | Easy, stats{4, 6, 6, 6}, stats{2, 3, -0.41667, -1}},
		{Nightcore | HardRock, stats{4, 6, 6, 6}, stats{5.2, 8.4, 10.01667, 9.93333}},
		{DoubleTime | HardRock, stats{10, 10, 10, 10}, stats{10, 10, 11.08333, 11}},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.m), func(t *testing.T) {
			cs, hp, od, ar := tt.m.ApplyToStats(tt.s.cs, tt.s.hp, tt.s.od, tt.s.ar)
			assert.InDelta(t, tt.want.cs, cs, 0.0001)
			assert.InDelta(t, tt.want.hp, hp, 0.0001)
			assert.InDelta(t, tt.want.od, od, 0.0001)
			assert.InDelta(t, tt.want.ar, ar, 0.0001)
		})
	}
}

func TestMods_String(t *testing.T) {
	tests := []struct {
		m    Mods
		want string
	}{
		{NoMod, ""},
		{Nightcore | DoubleTime, "NC"},
		{Perfect | SuddenDeath, "PF"},
		{DoubleTime | SuddenDeath, "DTSD"},
		{^Mods(0), "NFEZHDHRNCRXHTFLSOAPPFTD"},
	}
	for _, tt := range tests {
		t.Run(tt.want, func(t *testing.T) {
			assert.Equal(t, tt.want, tt.m.String())
		})
	}
}

func TestModsFromString(t *testing.T) {
	tests := []struct {
		s       string
		want    Mods
		wantErr bool
	}{
		{"", NoMod, false},
		{"nm", NoMod, false},
		{"HDDT", Hidden | DoubleTime, false},
		{"+hd;hr;;dt", Hidden | HardRock | DoubleTime, false},
		{"hrnc", HardRock | Nightcore | DoubleTime, false},
		{"ezpf", Easy | Perfect | SuddenDeath, false},
		{"nfezhdrxhtflsoaptd", NoFail | Easy | Hidden | Relax | HalfTime |
			Flashlight | SpunOut | AutoPilot | TouchDevice, false},

		{"hdd", 0, true},
		{"wtfs", 0, true},
		{"???", NoMod, false},

		{"ezhr", 0, true},
		{"sdnf", 0, true},
		{"pfnf", 0, true},
		{"sdrx", 0, true},
		{"pfrx", 0, true},
		{"sdap", 0, true},
		{"pfap", 0, true},
		{"dtht", 0, true},
		{"ncht", 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			m, err := ModsFromString(tt.s)
			assert.Equal(t, tt.want, m)
			switch tt.wantErr {
			case true:
				assert.Error(t, err)
			case false:
				assert.NoError(t, err)
			}
		})
	}
}
