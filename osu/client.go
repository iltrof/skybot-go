//go:generate mockery -name=cache -case=underscore -inpkg

package osu

import (
	"net/http"
	"time"

	mycache "github.com/iltrof/skybot/cache"
)

const defaultAPIBase = `http://osu.ppy.sh`

// Client is a client for making osu!API requests.
type Client struct {
	apiToken string
	apiBase  string // modified in tests
	httpCli  *http.Client
	cache    cache
}

// NewClient creates a new client.
func NewClient(apiToken string, httpClient *http.Client) *Client {
	return &Client{
		apiToken: apiToken,
		apiBase:  defaultAPIBase,
		httpCli:  httpClient,
		cache:    mycache.New(),
	}
}

type cache interface {
	Set(string, interface{}, time.Duration)
	Get(string) (interface{}, bool)
}
