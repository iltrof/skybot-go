package osu

import (
	"math"
)

// PPCalculator is used to calculate pp.
type PPCalculator struct {
	mods        Mods
	objectCount int
	circleCount int
	mapMaxCombo int
	aimStars    float64
	speedStars  float64
	ar          float64
	od          float64
}

// NewPPCalc creates a new calculator for the given map.
func NewPPCalc(b *Beatmap) *PPCalculator {
	return &PPCalculator{
		mods:        b.Mods,
		objectCount: b.CircleCount + b.SliderCount + b.SpinnerCount,
		circleCount: b.CircleCount,
		mapMaxCombo: b.MaxCombo,
		aimStars:    b.AimStars,
		speedStars:  b.SpeedStars,
		ar:          msToAR(ppARToMS(b.baseAR, b.Mods)),
		od:          ppMSToOD(ppODToMS(b.baseOD, b.Mods)),
	}
}

// SS returns pp for SS.
func (c *PPCalculator) SS() float64 {
	return c.calcPP(c.mapMaxCombo, 0, 0, 0)
}

// With returns pp for a play with given combo, n100, n50 and misses.
func (c *PPCalculator) With(combo, count100, count50, missCount int) float64 {
	if combo == 0 {
		combo = c.mapMaxCombo
	}
	return c.calcPP(combo, count100, count50, missCount)
}

// https://github.com/ppy/osu-performance/blob/master/src/performance/osu/OsuScore.cpp
func (c *PPCalculator) calcPP(maxCombo, count100, count50, missCount int) float64 {
	count300 := c.objectCount - count100 - count50 - missCount
	accuracy := float64(300*count300+100*count100+50*count50) / float64(300*c.objectCount)

	multiplier := 1.12
	if c.mods&NoFail != 0 {
		multiplier *= 0.9
	}
	if c.mods&SpunOut != 0 {
		multiplier *= 0.95
	}

	aim := c.calcAimValue(accuracy, maxCombo, missCount)
	speed := c.calcSpeedValue(accuracy, maxCombo, missCount)
	acc := c.calcAccValue(count300, count100, count50)
	return multiplier * math.Pow(
		math.Pow(aim, 1.1)+math.Pow(speed, 1.1)+math.Pow(acc, 1.1), 1/1.1)
}

func (c *PPCalculator) calcAimValue(accuracy float64, maxCombo, missCount int) float64 {
	rawAim := c.aimStars
	if c.mods&TouchDevice != 0 {
		rawAim = math.Pow(rawAim, 0.8)
	}

	aimValue := math.Pow(5*math.Max(1, rawAim/0.0675)-4, 3) / 100000
	lengthBonus := 0.95 + 0.4*math.Min(1, float64(c.objectCount)/2000)
	if c.objectCount > 2000 {
		lengthBonus += math.Log10(float64(c.objectCount)/2000) * 0.5
	}
	aimValue *= lengthBonus
	aimValue *= math.Pow(0.97, float64(missCount))

	if c.mapMaxCombo > 0 {
		aimValue *= math.Min(math.Pow(float64(maxCombo), 0.8)/
			math.Pow(float64(c.mapMaxCombo), 0.8), 1)
	}

	arFactor := 1.0
	switch {
	case c.ar > 10.33:
		arFactor += 0.3 * (c.ar - 10.33)
	case c.ar < 8:
		arFactor += 0.01 * (8 - c.ar)
	}
	aimValue *= arFactor

	if c.mods&Hidden != 0 {
		aimValue *= 1 + 0.04*(12-c.ar)
	}

	if c.mods&Flashlight != 0 {
		flFactor := 1 + 0.35*math.Min(1, float64(c.objectCount)/200)
		if c.objectCount > 200 {
			flFactor += 0.3 * math.Min(1, float64(c.objectCount-200)/300)
		}
		if c.objectCount > 500 {
			flFactor += float64(c.objectCount-500) / 1200
		}
		aimValue *= flFactor
	}

	aimValue *= 0.5 + accuracy/2
	aimValue *= 0.98 + math.Pow(c.od, 2)/2500
	return aimValue
}

func (c *PPCalculator) calcSpeedValue(accuracy float64, maxCombo, missCount int) float64 {
	speedValue := math.Pow(5*math.Max(1, c.speedStars/0.0675)-4, 3) / 100000

	arFactor := 1.0
	if c.ar > 10.33 {
		arFactor += 0.3 * (c.ar - 10.33)
	}
	speedValue *= arFactor

	lengthBonus := 0.95 + 0.4*math.Min(1, float64(c.objectCount)/2000)
	if c.objectCount > 2000 {
		lengthBonus += math.Log10(float64(c.objectCount)/2000) * 0.5
	}
	speedValue *= lengthBonus
	speedValue *= math.Pow(0.97, float64(missCount))

	if c.mapMaxCombo > 0 {
		speedValue *= math.Min(math.Pow(float64(maxCombo), 0.8)/
			math.Pow(float64(c.mapMaxCombo), 0.8), 1)
	}

	if c.mods&Hidden != 0 {
		speedValue *= 1 + 0.04*(12-c.ar)
	}

	speedValue *= 0.02 + accuracy
	speedValue *= 0.96 + math.Pow(c.od, 2)/1600
	return speedValue
}

func (c *PPCalculator) calcAccValue(count300, count100, count50 int) float64 {
	betterAccPercentage := 0.0
	if c.circleCount > 0 {
		betterAccPercentage = float64((count300-(c.objectCount-c.circleCount))*6+count100*2+count50) /
			float64(c.circleCount*6)
	}
	if betterAccPercentage < 0 {
		betterAccPercentage = 0
	}

	accValue := math.Pow(1.52163, c.od) * math.Pow(betterAccPercentage, 24) * 2.83
	accValue *= math.Min(1.15, math.Pow(float64(c.circleCount)/1000, 0.3))

	if c.mods&Hidden != 0 {
		accValue *= 1.08
	}
	if c.mods&Flashlight != 0 {
		accValue *= 1.02
	}
	return accValue
}

// This is a mess of float32/float64/int weirdness,
// but it's consistent with osu!'s pp calculator.
func ppARToMS(ar float64, mods Mods) float64 {
	ar = float64(float32(ar))
	if mods&Easy != 0 {
		ar *= 0.5
	}
	if mods&HardRock != 0 {
		ar *= 1.4
	}
	if ar > 10 {
		ar = 10
	}

	var ms int
	switch {
	case ar < 5:
		ms = int(1800 - ar*120)
	default:
		ms = int(1200 - (ar-5)*150)
	}
	return float64(ms) / mods.SpeedMultiplier()
}

// This is a mess of float32/float64/int weirdness,
// but it's consistent with osu!'s pp calculator.
func ppODToMS(od float64, mods Mods) float64 {
	od = float64(float32(od))
	if mods&Easy != 0 {
		od *= 0.5
	}
	if mods&HardRock != 0 {
		od *= 1.4
	}
	if od > 10 {
		od = 10
	}

	ms := int(80 - od*6)
	return float64(ms) / mods.SpeedMultiplier()
}

func ppMSToOD(ms float64) float64 {
	return (80 - ms) / 6
}
