package osu

import (
	"fmt"
	"time"

	"golang.org/x/xerrors"
)

// Beatmap is an osu! beatmap.
type Beatmap struct {
	ID    int
	SetID int

	Artist   string
	Title    string
	Diff     string
	Mapper   string
	MapperID int
	Mode     int
	Status   int

	Mods Mods
	CS   float64
	OD   float64
	AR   float64
	HP   float64

	baseAR float64 // for pp calculation
	baseOD float64

	BPM          float64
	Length       time.Duration
	CircleCount  int
	SliderCount  int
	SpinnerCount int
	MaxCombo     int

	TotalStars float64
	AimStars   float64
	SpeedStars float64

	Downloadable bool

	pp *PPCalculator
}

// GetBeatmap returns the beatmap with the given ID.
func (c *Client) GetBeatmap(id string, mods Mods) (*Beatmap, error) {
	relevantMods := int(mods & DiffChanging)
	if cached, ok := c.cache.Get(fmt.Sprintf("b%s:%d", id, relevantMods)); ok {
		b := cached.(Beatmap)
		b.Mods = mods
		return &b, nil
	}

	b, err := c.getBeatmaps(fmt.Sprintf("%s/api/get_beatmaps?k=%s&b=%s&mods=%d",
		c.apiBase, c.apiToken, id, relevantMods), mods)
	if err != nil {
		return nil, err
	}
	if len(b) == 0 {
		return nil, nil
	}
	c.cache.Set(fmt.Sprintf("b%d:%d", b[0].ID, relevantMods), *b[0], time.Hour)
	return b[0], nil
}

// GetBeatmapSet returns the beatmap set with the given name.
func (c *Client) GetBeatmapSet(id string, mods Mods) ([]*Beatmap, error) {
	relevantMods := int(mods & DiffChanging)
	b, err := c.getBeatmaps(fmt.Sprintf("%s/api/get_beatmaps?k=%s&s=%s&mods=%d",
		c.apiBase, c.apiToken, id, relevantMods), mods)
	if err != nil {
		return nil, err
	}

	for _, b := range b {
		c.cache.Set(fmt.Sprintf("b%d:%d", b.ID, relevantMods), *b, time.Hour)
	}
	return b, nil
}

func (c *Client) getBeatmaps(url string, mods Mods) ([]*Beatmap, error) {
	var b []apiBeatmap
	err := c.getJSON(&b, url)
	if err != nil {
		return nil, xerrors.Errorf("get %s: %w", url, err)
	}
	if len(b) == 0 {
		return nil, nil
	}

	result := make([]*Beatmap, len(b))
	for i, b := range b {
		result[i] = b.beatmap(mods)
	}

	return result, nil
}

type apiBeatmap struct {
	BeatmapsetID        int     `json:"beatmapset_id,string"`
	BeatmapID           int     `json:"beatmap_id,string"`
	Approved            int     `json:"approved,string"`
	TotalLength         int     `json:"total_length,string"`
	HitLength           int     `json:"hit_length,string"`
	Version             string  `json:"version"`
	FileMD5             string  `json:"file_md5"`
	DiffSize            float64 `json:"diff_size,string"`
	DiffOverall         float64 `json:"diff_overall,string"`
	DiffApproach        float64 `json:"diff_approach,string"`
	DiffDrain           float64 `json:"diff_drain,string"`
	Mode                int     `json:"mode,string"`
	CountNormal         int     `json:"count_normal,string"`
	CountSlider         int     `json:"count_slider,string"`
	CountSpinner        int     `json:"count_spinner,string"`
	SubmitDate          apiDate `json:"submit_date"`
	ApprovedDate        apiDate `json:"approved_date"`
	LastUpdate          apiDate `json:"last_update"`
	Artist              string  `json:"artist"`
	Title               string  `json:"title"`
	Creator             string  `json:"creator"`
	CreatorID           int     `json:"creator_id,string"`
	BPM                 float64 `json:"bpm,string"`
	Source              string  `json:"source"`
	Tags                string  `json:"tags"`
	GenreID             int     `json:"genre_id,string"`
	LanguageID          int     `json:"language_id,string"`
	FavouriteCount      int     `json:"favourite_count,string"`
	Rating              float64 `json:"rating,string"`
	DownloadUnavailable apiBool `json:"download_unavailable"`
	AudioUnavailable    apiBool `json:"audio_unavailable"`
	Playcount           int     `json:"playcount,string"`
	Passcount           int     `json:"passcount,string"`
	MaxCombo            int     `json:"max_combo,string"`
	DiffAim             float64 `json:"diff_aim,string"`
	DiffSpeed           float64 `json:"diff_speed,string"`
	Difficultyrating    float64 `json:"difficultyrating,string"`
}

func (b *apiBeatmap) beatmap(mods Mods) *Beatmap {
	cs, hp, od, ar := mods.ApplyToStats(b.DiffSize, b.DiffDrain, b.DiffOverall, b.DiffApproach)
	return &Beatmap{
		ID:    b.BeatmapID,
		SetID: b.BeatmapsetID,

		Artist:   b.Artist,
		Title:    b.Title,
		Diff:     b.Version,
		Mapper:   b.Creator,
		MapperID: b.CreatorID,
		Mode:     b.Mode,
		Status:   b.Approved,

		Mods: mods,
		CS:   cs,
		OD:   od,
		AR:   ar,
		HP:   hp,

		baseAR: b.DiffApproach,
		baseOD: b.DiffOverall,

		BPM:          b.BPM * mods.SpeedMultiplier(),
		Length:       time.Duration(float64(b.TotalLength)/mods.SpeedMultiplier()) * time.Second,
		CircleCount:  b.CountNormal,
		SliderCount:  b.CountSlider,
		SpinnerCount: b.CountSpinner,
		MaxCombo:     b.MaxCombo,

		TotalStars: b.Difficultyrating,
		AimStars:   b.DiffAim,
		SpeedStars: b.DiffSpeed,

		Downloadable: !bool(b.DownloadUnavailable),
	}
}

// PP returns the associated pp calculator.
func (b *Beatmap) PP() *PPCalculator {
	if b.pp == nil {
		b.pp = NewPPCalc(b)
	}
	return b.pp
}

// URL returns the URL to the map's page.
func (b *Beatmap) URL() string {
	return fmt.Sprintf("https://osu.ppy.sh/beatmaps/%d", b.ID)
}

// DownloadURL returns the download URL for the map.
func (b *Beatmap) DownloadURL() string {
	if !b.Downloadable {
		return ""
	}
	return fmt.Sprintf("https://osu.ppy.sh/beatmapsets/%d/download", b.SetID)
}

// ThumbnailURL returns map's thumbnail URL.
func (b *Beatmap) ThumbnailURL() string {
	return fmt.Sprintf("https://assets.ppy.sh/beatmaps/%d/covers/list@2x.jpg", b.SetID)
}

// MapperProfileURL returns the profile URL of the mapper.
func (b *Beatmap) MapperProfileURL() string {
	return (&User{ID: b.MapperID}).ProfileURL()
}

// ByDifficulty implements sorting beatmaps by difficulty.
type ByDifficulty []*Beatmap

func (a ByDifficulty) Len() int           { return len(a) }
func (a ByDifficulty) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDifficulty) Less(i, j int) bool { return a[i].TotalStars < a[j].TotalStars }
