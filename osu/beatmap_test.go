package osu

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/stretchr/testify/mock"
)

func (s *getterSuite) TestClient_getBeatmaps() {
	tests := []struct {
		name     string
		mods     Mods
		response string
		want     []*Beatmap
	}{
		{"empty response", NoMod, `[]`, nil},
		{"normal", NoMod, sampleBeatmapResponse, []*Beatmap{sampleBeatmap}},
		{"hdhrdt", Hidden | HardRock | DoubleTime,
			sampleBeatmapResponseHDHRDT, []*Beatmap{sampleBeatmapHDHRDT}},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			requests := 0
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				requests++
				fmt.Fprint(w, tt.response)
			}

			u, err := s.client.getBeatmaps(s.server.URL, tt.mods)
			s.NoError(err)
			s.Equal(tt.want, u)
			s.Equal(1, requests)
		})
	}
}

func (s *getterSuite) TestClient_GetBeatmap() {
	wantPath := `/api/get_beatmaps`
	tests := []struct {
		name       string
		mods       Mods
		response   string
		want       *Beatmap
		wantValues url.Values
	}{
		{"empty response", NoMod, `[]`, nil,
			url.Values{"k": {"xxx"}, "b": {strconv.Itoa(sampleBeatmap.ID)}, "mods": {"0"}}},
		{"normal", NoMod, sampleBeatmapResponse, sampleBeatmap,
			url.Values{"k": {"xxx"}, "b": {strconv.Itoa(sampleBeatmap.ID)}, "mods": {"0"}}},
		{"hdhrdt", Hidden | HardRock | DoubleTime, `[]`, nil,
			url.Values{"k": {"xxx"}, "b": {strconv.Itoa(sampleBeatmap.ID)}, "mods": {"80"}}},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			s.cache.On("Get", mock.Anything).Return(nil, false)
			s.cache.On("Set", mock.Anything, mock.Anything, mock.Anything)
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				s.Equal(wantPath, r.URL.Path)
				s.Equal(tt.wantValues, r.URL.Query())
				fmt.Fprint(w, tt.response)
			}

			b, err := s.client.GetBeatmap(strconv.Itoa(sampleBeatmap.ID), tt.mods)
			s.NoError(err)
			s.Equal(tt.want, b)
		})
	}
}

func (s *getterSuite) TestClient_GetBeatmap_caching() {
	tests := []struct {
		name     string
		mods     Mods
		response string
		beatmap  *Beatmap
		cacheKey string
	}{
		{"nomod", NoMod, sampleBeatmapResponse, sampleBeatmap, "b75:0"},
		{"hdhrdt", Hidden | HardRock | DoubleTime,
			sampleBeatmapResponseHDHRDT, sampleBeatmapHDHRDT, "b75:80"},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			requests := 0
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				requests++
				fmt.Fprint(w, tt.response)
			}

			s.cache.On("Get", tt.cacheKey).Return(nil, false).Once()
			s.cache.On("Set", tt.cacheKey, *tt.beatmap, time.Hour).Once()
			_, _ = s.client.GetBeatmap("75", tt.mods)

			s.cache.On("Get", tt.cacheKey).Return(*tt.beatmap, true).Once()
			_, _ = s.client.GetBeatmap("75", tt.mods)

			s.Equal(1, requests)
		})
	}
	s.cache.AssertExpectations(s.T())
}

func (s *getterSuite) TestClient_GetBeatmap_cachedModsUpdated() {
	s.hand = func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, sampleBeatmapResponseHDHRDT)
	}

	s.cache.On("Get", "b75:80").Return(nil, false).Once()
	s.cache.On("Set", "b75:80", *sampleBeatmapHDHRDT, time.Hour).Once()
	_, _ = s.client.GetBeatmap("75", Hidden|HardRock|DoubleTime)

	s.cache.On("Get", "b75:80").Return(*sampleBeatmapHDHRDT, true).Once()
	b, _ := s.client.GetBeatmap("75", HardRock|DoubleTime)
	s.Equal(HardRock|DoubleTime, b.Mods)
}

func (s *getterSuite) TestClient_GetBeatmapSet() {
	wantPath := `/api/get_beatmaps`
	tests := []struct {
		name       string
		mods       Mods
		response   string
		want       []*Beatmap
		wantValues url.Values
	}{
		{"empty response", NoMod, `[]`, nil,
			url.Values{"k": {"xxx"}, "s": {strconv.Itoa(sampleBeatmap.SetID)}, "mods": {"0"}}},
		{"normal", NoMod, sampleBeatmapResponse, []*Beatmap{sampleBeatmap},
			url.Values{"k": {"xxx"}, "s": {strconv.Itoa(sampleBeatmap.SetID)}, "mods": {"0"}}},
		{"hdhrdt", Hidden | HardRock | DoubleTime, `[]`, nil,
			url.Values{"k": {"xxx"}, "s": {strconv.Itoa(sampleBeatmap.SetID)}, "mods": {"80"}}},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			s.cache.On("Set", mock.Anything, mock.Anything, mock.Anything)
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				s.Equal(wantPath, r.URL.Path)
				s.Equal(tt.wantValues, r.URL.Query())
				fmt.Fprint(w, tt.response)
			}

			b, err := s.client.GetBeatmapSet(strconv.Itoa(sampleBeatmap.SetID), tt.mods)
			s.NoError(err)
			s.Equal(tt.want, b)
		})
	}
}

func (s *getterSuite) TestClient_GetBeatmapSet_caching() {
	s.hand = func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `[{"beatmap_id": "42"}, {"beatmap_id": "77"}]`)
	}
	s.cache.On("Set", "b42:0", Beatmap{ID: 42, Downloadable: true}, time.Hour).Once()
	s.cache.On("Set", "b77:0", Beatmap{ID: 77, Downloadable: true}, time.Hour).Once()
	_, _ = s.client.GetBeatmapSet("", NoMod)

	s.hand = func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, sampleBeatmapResponseHDHRDT)
	}
	s.cache.On("Set", "b75:80", *sampleBeatmapHDHRDT, time.Hour).Once()
	_, _ = s.client.GetBeatmapSet("", Hidden|HardRock|DoubleTime)

	s.cache.AssertExpectations(s.T())
}

var sampleBeatmap = &Beatmap{
	ID:    75,
	SetID: 1,

	Artist:   "Kenji Ninuma",
	Title:    "DISCO PRINCE",
	Diff:     "Normal",
	Mapper:   "peppy",
	MapperID: 2,
	Mode:     0,
	Status:   1,

	Mods: NoMod,
	CS:   4,
	OD:   6,
	AR:   6,
	HP:   6,

	baseAR: 6,
	baseOD: 6,

	BPM:          119.999,
	Length:       142 * time.Second,
	CircleCount:  160,
	SliderCount:  0,
	SpinnerCount: 0,
	MaxCombo:     314,

	TotalStars: 2.4069502353668213,
	AimStars:   1.1959426403045654,
	SpeedStars: 1.2059860229492188,

	Downloadable: true,
}

const sampleBeatmapResponse = `[
	{
		"beatmapset_id": "1",
		"beatmap_id": "75",
		"approved": "1",
		"total_length": "142",
		"hit_length": "109",
		"version": "Normal",
		"file_md5": "a5b99395a42bd55bc5eb1d2411cbdf8b",
		"diff_size": "4",
		"diff_overall": "6",
		"diff_approach": "6",
		"diff_drain": "6",
		"mode": "0",
		"count_normal": "160",
		"count_slider": "0",
		"count_spinner": "0",
		"submit_date": "2007-10-06 17:46:31",
		"approved_date": "2007-10-06 17:46:31",
		"last_update": "2007-10-06 17:46:31",
		"artist": "Kenji Ninuma",
		"title": "DISCO PRINCE",
		"creator": "peppy",
		"creator_id": "2",
		"bpm": "119.999",
		"source": "",
		"tags": "katamari",
		"genre_id": "2",
		"language_id": "3",
		"favourite_count": "519",
		"rating": "8.12836",
		"download_unavailable": "0",
		"audio_unavailable": "0",
		"playcount": "370967",
		"passcount": "46472",
		"max_combo": "314",
		"diff_aim": "1.1959426403045654",
		"diff_speed": "1.2059860229492188",
		"difficultyrating": "2.4069502353668213"
	}
]`

var sampleBeatmapHDHRDT = &Beatmap{
	ID:    75,
	SetID: 1,

	Artist:   "Kenji Ninuma",
	Title:    "DISCO PRINCE",
	Diff:     "Normal",
	Mapper:   "peppy",
	MapperID: 2,
	Mode:     0,
	Status:   1,

	Mods: Hidden | HardRock | DoubleTime,
	CS:   5.2,
	OD:   10.016666666666666,
	AR:   9.933333333333332,
	HP:   8.399999999999999,

	baseAR: 6,
	baseOD: 6,

	BPM:          179.99849999999998,
	Length:       94 * time.Second,
	CircleCount:  160,
	SliderCount:  0,
	SpinnerCount: 0,
	MaxCombo:     314,

	TotalStars: 3.7178964614868164,
	AimStars:   1.8731626272201538,
	SpeedStars: 1.8163048028945923,

	Downloadable: true,
}

const sampleBeatmapResponseHDHRDT = `[
	{
		"beatmapset_id": "1",
		"beatmap_id": "75",
		"approved": "1",
		"total_length": "142",
		"hit_length": "109",
		"version": "Normal",
		"file_md5": "a5b99395a42bd55bc5eb1d2411cbdf8b",
		"diff_size": "4",
		"diff_overall": "6",
		"diff_approach": "6",
		"diff_drain": "6",
		"mode": "0",
		"count_normal": "160",
		"count_slider": "0",
		"count_spinner": "0",
		"submit_date": "2007-10-06 17:46:31",
		"approved_date": "2007-10-06 17:46:31",
		"last_update": "2007-10-06 17:46:31",
		"artist": "Kenji Ninuma",
		"title": "DISCO PRINCE",
		"creator": "peppy",
		"creator_id": "2",
		"bpm": "119.999",
		"source": "",
		"tags": "katamari",
		"genre_id": "2",
		"language_id": "3",
		"favourite_count": "519",
		"rating": "8.12836",
		"download_unavailable": "0",
		"audio_unavailable": "0",
		"playcount": "370967",
		"passcount": "46472",
		"max_combo": "314",
		"diff_aim": "1.8731626272201538",
		"diff_speed": "1.8163048028945923",
		"difficultyrating": "3.7178964614868164"
	}
]`
