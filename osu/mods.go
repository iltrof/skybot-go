package osu

import (
	"strings"
	"unicode"

	"golang.org/x/xerrors"
)

// Mods represents a mod combination.
type Mods uint64

// Relevant mods.
const (
	NoMod       Mods = 0
	NoFail      Mods = 1 << 0
	Easy        Mods = 1 << 1
	TouchDevice Mods = 1 << 2
	Hidden      Mods = 1 << 3
	HardRock    Mods = 1 << 4
	SuddenDeath Mods = 1 << 5
	DoubleTime  Mods = 1 << 6
	Relax       Mods = 1 << 7
	HalfTime    Mods = 1 << 8
	Nightcore   Mods = 1 << 9
	Flashlight  Mods = 1 << 10
	SpunOut     Mods = 1 << 12
	AutoPilot   Mods = 1 << 13
	Perfect     Mods = 1 << 14

	DiffChanging Mods = Easy | HardRock | DoubleTime | HalfTime
)

// SpeedMultiplier returns the speed multiplier for the mod combination.
func (m Mods) SpeedMultiplier() float64 {
	switch {
	case m&(DoubleTime|Nightcore) != 0:
		return 1.5
	case m&HalfTime != 0:
		return 0.75
	default:
		return 1
	}
}

// ApplyToStats recalculates the given stats
// using the mod combination.
func (m Mods) ApplyToStats(cs, hp, od, ar float64) (newCS, newHP, newOD, newAR float64) {
	newCS, newHP, newOD, newAR = cs, hp, od, ar
	if m&Easy != 0 {
		newCS *= 0.5
		newHP *= 0.5
		newOD *= 0.5
		newAR *= 0.5
	}
	if m&HardRock != 0 {
		newCS = clamp(newCS*1.3, 10)
		newHP = clamp(newHP*1.4, 10)
		newOD = clamp(newOD*1.4, 10)
		newAR = clamp(newAR*1.4, 10)
	}

	newAR = msToAR(arToMS(newAR) / m.SpeedMultiplier())
	newOD = msToOD(odToMS(newOD) / m.SpeedMultiplier())
	return
}

func clamp(v, max float64) float64 {
	if v > max {
		return max
	}
	return v
}

func arToMS(ar float64) float64 {
	switch {
	case ar < 5:
		return 1800 - ar*120
	default:
		return 1200 - (ar-5)*150
	}
}

func msToAR(ms float64) float64 {
	switch {
	case ms > 1200:
		return (1800 - ms) / 120
	default:
		return 5 + (1200-ms)/150
	}
}

func odToMS(od float64) float64 {
	return 79.5 - od*6
}

func msToOD(ms float64) float64 {
	return (79.5 - ms) / 6
}

// https://github.com/ppy/osu-web/blob/master/app/Libraries/ModsHelper.php
var modNames = []struct {
	mod  Mods
	name string
}{
	{NoFail, "NF"}, {Easy, "EZ"}, {Hidden, "HD"}, {HardRock, "HR"},
	{Nightcore, "NC"}, {DoubleTime, "DT"}, {Relax, "RX"}, {HalfTime, "HT"},
	{Flashlight, "FL"}, {SpunOut, "SO"}, {AutoPilot, "AP"},
	{Perfect, "PF"}, {SuddenDeath, "SD"}, {TouchDevice, "TD"},
}

func (m Mods) String() string {
	if m&Nightcore != 0 {
		m &^= DoubleTime
	}
	if m&Perfect != 0 {
		m &^= SuddenDeath
	}
	b := strings.Builder{}
	for _, conv := range modNames {
		if m&conv.mod != 0 {
			b.WriteString(conv.name)
		}
	}
	return b.String()
}

var stringToMod = map[string]Mods{
	"nm": NoMod, "nf": NoFail, "ez": Easy, "hd": Hidden,
	"hr": HardRock, "nc": Nightcore, "dt": DoubleTime, "rx": Relax,
	"ht": HalfTime, "fl": Flashlight, "so": SpunOut, "ap": AutoPilot,
	"pf": Perfect, "sd": SuddenDeath, "td": TouchDevice,
}

var invalidMods = []Mods{
	Easy | HardRock, SuddenDeath | NoFail,
	SuddenDeath | Relax, SuddenDeath | AutoPilot,
	DoubleTime | HalfTime,
}

// ModsFromString turns a string of two-letter mod names
// into a mod combination. Case-insensitive, all non-letters
// are ignored, so "HDDT" and "+hd,hr,dt" would be both valid.
// Returns an error if an unknown mod is encountered or the
// mod combination is invalid (ezhr, sdnf, sdrx, sdap, dtht).
func ModsFromString(s string) (Mods, error) {
	b := strings.Builder{}
	for _, r := range s {
		r = unicode.ToLower(r)
		if r < 'a' || r > 'z' {
			continue
		}
		b.WriteRune(r)
	}

	norm := b.String()
	if len(norm)%2 != 0 {
		return 0, xerrors.Errorf("number of letters should be even")
	}

	var result Mods
	for i := 0; i < len(norm); i += 2 {
		m, ok := stringToMod[norm[i:i+2]]
		if !ok {
			return 0, xerrors.Errorf("unknown mod: %s", norm[i:i+2])
		}
		result |= m
	}

	if result&Nightcore != 0 {
		result |= DoubleTime
	}
	if result&Perfect != 0 {
		result |= SuddenDeath
	}

	for _, im := range invalidMods {
		if result&im == im {
			return 0, xerrors.Errorf("invalid mod combination: %s", result)
		}
	}
	return result, nil
}
