package osu

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/iltrof/skybot/friendlyerr"
	"golang.org/x/xerrors"
)

func (c *Client) getJSON(dest interface{}, rawurl string) error {
	r, err := c.httpCli.Get(rawurl)
	if err != nil {
		return friendlyerr.Wrapf(
			xerrors.Errorf("connection failed: %w", err),
			serverUnavailable,
		)
	}
	defer r.Body.Close()

	if r.StatusCode != http.StatusOK {
		var err error
		msg := getErrorMessage(r.Body)
		if msg == "" {
			err = xerrors.Errorf("request failed: %s", r.Status)
		} else {
			err = xerrors.Errorf("request failed: %s (%q)", r.Status, msg)
		}

		if r.StatusCode >= 400 && r.StatusCode < 500 {
			return friendlyerr.Wrapf(err, serverRejecting)
		}
		return friendlyerr.Wrapf(err, serverUnavailable)
	}

	d := json.NewDecoder(r.Body)
	err = d.Decode(dest)
	if err != nil {
		return friendlyerr.Wrapf(err, serverBadResponse)
	}
	return nil
}

func getErrorMessage(r io.Reader) string {
	d := json.NewDecoder(r)
	var e struct {
		Error string `json:"error"`
	}
	err := d.Decode(&e)
	if err != nil {
		return ""
	}
	return e.Error
}

const osuapiTimeFormat = "2006-01-02 15:04:05"

type apiDate struct {
	time.Time
}

func (t *apiDate) UnmarshalJSON(b []byte) error {
	var v *string
	if err := json.Unmarshal(b, &v); err != nil {
		return xerrors.Errorf("unmarshal: %w", err)
	}
	if v == nil {
		return nil
	}

	var err error
	t.Time, err = time.Parse(osuapiTimeFormat, *v)
	if err != nil {
		return xerrors.Errorf("parse time: %w", err)
	}
	return nil
}

// JSON representation: string with a number of seconds inside.
type apiSeconds struct {
	time.Duration
}

func (s *apiSeconds) UnmarshalJSON(b []byte) error {
	var v *string
	if err := json.Unmarshal(b, &v); err != nil {
		return xerrors.Errorf("unmarshal: %w", err)
	}
	if v == nil {
		return nil
	}

	var err error
	s.Duration, err = time.ParseDuration(*v + "s")
	if err != nil {
		return xerrors.Errorf("parse time: %w", err)
	}
	return nil
}

// JSON representation: "0" = false, any other string = true.
type apiBool bool

func (b *apiBool) UnmarshalJSON(bs []byte) error {
	var v *string
	if err := json.Unmarshal(bs, &v); err != nil {
		return xerrors.Errorf("unmarshal: %w", err)
	}
	if v == nil {
		return nil
	}

	*b = true
	if *v == "0" {
		*b = false
	}
	return nil
}

const (
	serverUnavailable = "osu! servers seem to be unavailable, maybe try again later?"
	serverRejecting   = "osu! servers aren't letting me connect to them. " +
		"The issue probably won't fix itself, but you can always try again later."
	serverBadResponse = "osu! servers are responding weirdly. " +
		"The issue probably won't fix itself, but you can always try again later."
)
