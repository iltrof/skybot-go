package osu

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type getterSuite struct {
	suite.Suite
	server *httptest.Server
	hand   http.HandlerFunc
	client *Client
	cache  mockCache
}

func (s *getterSuite) SetupTest() {
	s.server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.hand(w, r)
	}))
	s.client = NewClient("xxx", &http.Client{})
	s.client.apiBase = s.server.URL
	s.cache = mockCache{}
	s.client.cache = &s.cache
}

func (s *getterSuite) TearDownTest() {
	s.server.Close()
}

func TestGetters(t *testing.T) {
	suite.Run(t, &getterSuite{})
}
