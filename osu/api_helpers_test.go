package osu

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/iltrof/skybot/friendlyerr"
	"github.com/stretchr/testify/assert"
)

func Test_getJSON_errors(t *testing.T) {
	var dummy struct{}
	client := NewClient("xxx", &http.Client{})

	t.Run("connection error", func(t *testing.T) {
		c := NewClient("xxx", &http.Client{Timeout: 1 * time.Nanosecond})
		err := c.getJSON(&dummy, "http://10.255.255.1") // something unreachable
		assert.Error(t, err)
		assert.Equal(t, serverUnavailable, friendlyerr.Description(err))
	})
	t.Run("non-json response", func(t *testing.T) {
		s := newTestServer(`<html></html>`)
		defer s.Close()
		err := client.getJSON(&dummy, s.URL)
		assert.Error(t, err)
		assert.Equal(t, serverBadResponse, friendlyerr.Description(err))
	})
	t.Run("client-side error", func(t *testing.T) {
		s := newTestServerWithStatus(``, http.StatusBadRequest)
		defer s.Close()
		err := client.getJSON(&dummy, s.URL)
		assert.Error(t, err)
		assert.Equal(t, serverRejecting, friendlyerr.Description(err))
	})
	t.Run("client-side error with message", func(t *testing.T) {
		s := newTestServerWithStatus(`{"error":"Please provide a valid API key."}`, http.StatusUnauthorized)
		defer s.Close()
		err := client.getJSON(&dummy, s.URL)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), "Please provide a valid API key.")
		assert.Equal(t, serverRejecting, friendlyerr.Description(err))
	})
	t.Run("server-side error", func(t *testing.T) {
		s := newTestServerWithStatus(``, http.StatusBadGateway)
		defer s.Close()
		err := client.getJSON(&dummy, s.URL)
		assert.Error(t, err)
		assert.Equal(t, serverUnavailable, friendlyerr.Description(err))
	})
}

func newTestServer(response string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, response)
	}))
}

func newTestServerWithStatus(response string, status int) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, response, status)
	}))
}
func Test_apiDate_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    apiDate
		wantErr bool
	}{
		{"normal", `"2019-01-16 12:09:42"`, apiDate{time.Date(2019, 1, 16, 12, 9, 42, 0, time.UTC)}, false},
		{"bad string", `"2019-jan-whatever"`, apiDate{}, true},
		{"non-string", `2`, apiDate{}, true},
		{"null", `null`, apiDate{}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got apiDate
			err := got.UnmarshalJSON([]byte(tt.json))
			assert.Equal(t, err != nil, tt.wantErr)
			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_apiSeconds_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    apiSeconds
		wantErr bool
	}{
		{"normal", `"72"`, apiSeconds{72 * time.Second}, false},
		{"bad string", `"xx"`, apiSeconds{}, true},
		{"non-string", `70`, apiSeconds{}, true},
		{"null", `null`, apiSeconds{}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got apiSeconds
			err := got.UnmarshalJSON([]byte(tt.json))
			assert.Equal(t, err != nil, tt.wantErr)
			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_apiBool_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    apiBool
		wantErr bool
	}{
		{"false", `"0"`, apiBool(false), false},
		{"true", `"1"`, apiBool(true), false},
		{"truey", `"abc"`, apiBool(true), false},
		{"null", `null`, apiBool(false), false},
		{"non-string", `abc`, apiBool(false), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got apiBool
			err := got.UnmarshalJSON([]byte(tt.json))
			assert.Equal(t, err != nil, tt.wantErr)
			assert.Equal(t, tt.want, got)
		})
	}
}
