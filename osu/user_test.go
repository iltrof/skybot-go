package osu

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func (s *getterSuite) TestClient_getUser() {
	tests := []struct {
		name     string
		response string
		want     *User
	}{
		{"empty response", `[]`, nil},
		{"normal", sampleUserResponse, sampleUser},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			requests := 0
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				requests++
				fmt.Fprint(w, tt.response)
			}

			u, err := s.client.getUser(s.server.URL)
			s.NoError(err)
			s.Equal(tt.want, u)
			s.Equal(1, requests)
		})
	}
}

func (s *getterSuite) TestClient_GetUserBy() {
	wantPath := `/api/get_user`
	tests := []struct {
		name       string
		get        func() (*User, error)
		wantValues url.Values
	}{
		{
			"id", func() (*User, error) {
				return s.client.GetUserByID(strconv.Itoa(sampleUser.ID))
			}, url.Values{"k": {"xxx"}, "type": {"id"}, "u": {strconv.Itoa(sampleUser.ID)}},
		},
		{
			"name", func() (*User, error) {
				return s.client.GetUserByName(sampleUser.Name)
			}, url.Values{"k": {"xxx"}, "type": {"string"}, "u": {sampleUser.Name}},
		},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			s.hand = func(w http.ResponseWriter, r *http.Request) {
				s.Equal(wantPath, r.URL.Path)
				s.Equal(tt.wantValues, r.URL.Query())
				fmt.Fprint(w, sampleUserResponse)
			}

			u, err := tt.get()
			s.NoError(err)
			s.Equal(sampleUser, u)
		})
	}
}

func TestUser_ProfileURL(t *testing.T) {
	want := fmt.Sprintf("https://osu.ppy.sh/users/%d", sampleUser.ID)
	assert.Equal(t, want, sampleUser.ProfileURL())
}

var sampleUser = &User{
	ID:             39828,
	Name:           "WubWoofWolf",
	CountryCode:    "PL",
	AvatarURL:      "http://a.ppy.sh/39828",
	PP:             12578.1,
	Rank:           45,
	CountryRank:    3,
	Accuracy:       99.17985534667969,
	RankedPlays:    267599,
	RankedPlayTime: 20078383 * time.Second,
	Level:          108.236,
	CountSS:        2994,
	CountSSH:       7560,
	CountS:         781,
	CountSH:        9700,
	CountA:         2597,
}

const sampleUserResponse = `[
    {
        "user_id": "39828",
        "username": "WubWoofWolf",
        "join_date": "2008-09-19 13:04:36",
        "count300": "56784067",
        "count100": "3380742",
        "count50": "599964",
        "playcount": "267599",
        "ranked_score": "210720024070",
        "total_score": "850541626548",
        "pp_rank": "45",
        "level": "108.236",
        "pp_raw": "12578.1",
        "accuracy": "99.17985534667969",
        "count_rank_ss": "2994",
        "count_rank_ssh": "7560",
        "count_rank_s": "781",
        "count_rank_sh": "9700",
        "count_rank_a": "2597",
        "country": "PL",
        "total_seconds_played": "20078383",
        "pp_country_rank": "3",
        "events": [
            {
                "display_html": "<b><a href='/u/39828'>WubWoofWolf</a></b> has lost first place on <a href='/b/27124?m=0'>Chiaki Ishikawa - Uninstall [v2b's Jumping Madness!]</a> (osu!)",
                "beatmap_id": "27124",
                "beatmapset_id": "5480",
                "date": "2019-03-27 11:44:40",
                "epicfactor": "2"
            }
        ]
    }
]`
