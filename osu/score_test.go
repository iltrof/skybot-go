//nolint
package osu

import "time"

var sampleUserRecentScore = &Score{
	BeatmapID: 33415,
	Score:     306963,
	Rank:      "F",
	PP:        0,

	Combo:     81,
	Count300:  166,
	Count100:  27,
	Count50:   10,
	MissCount: 20,
	FC:        false,

	Mods: Hidden | DoubleTime,

	UserID:      2558286,
	Username:    "",
	CountryCode: "",

	Date: time.Date(2019, time.August, 24, 21, 23, 11, 0, time.UTC),
}

const sampleUserRecentResponse = `[
	{
		"beatmap_id": "33415",
		"score": "306963",
		"maxcombo": "81",
		"count50": "10",
		"count100": "27",
		"count300": "166",
		"countmiss": "20",
		"countkatu": "5",
		"countgeki": "21",
		"perfect": "0",
		"enabled_mods": "72",
		"user_id": "2558286",
		"date": "2019-08-24 21:23:11",
		"rank": "F"
	}
]`
