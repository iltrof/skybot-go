package osu

import "time"

// Score represents a score in osu!
type Score struct {
	BeatmapID int
	Score     int64
	Rank      string
	PP        float64

	Combo     int
	Count300  int
	Count100  int
	Count50   int
	MissCount int
	FC        bool

	Mods Mods

	UserID      int
	Username    string
	CountryCode string

	Date time.Time
}

//nolint:unused
type apiScore struct {
	BeatmapID   int     `json:"beatmap_id,string"`
	ScoreID     int     `json:"score_id,string"`
	Score       int64   `json:"score,string"`
	Maxcombo    int     `json:"maxcombo,string"`
	Count50     int     `json:"count50,string"`
	Count100    int     `json:"count100,string"`
	Count300    int     `json:"count300,string"`
	Countmiss   int     `json:"countmiss,string"`
	Countkatu   int     `json:"countkatu,string"`
	Countgeki   int     `json:"countgeki,string"`
	Perfect     apiBool `json:"perfect"`
	EnabledMods int     `json:"enabledMods,string"`
	UserID      int     `json:"user_id,string"`
	Username    string  `json:"username"`
	Date        apiDate `json:"date"`
	Rank        string  `json:"rank"`
	PP          float64 `json:"pp,string"`
}

func (s *apiScore) score() *Score {
	return &Score{
		BeatmapID: s.BeatmapID,
		Score:     s.Score,
		Rank:      s.Rank,
		PP:        s.PP,

		Combo:     s.Maxcombo,
		Count300:  s.Count300,
		Count100:  s.Count100,
		Count50:   s.Count50,
		MissCount: s.Countmiss,
		FC:        bool(s.Perfect),

		Mods: Mods(s.EnabledMods),

		UserID:      s.UserID,
		Username:    s.Username,
		CountryCode: "",

		Date: s.Date.Time,
	}
}
