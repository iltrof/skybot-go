import requests
import os
import sys
import time
from dataclasses import dataclass

if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} output-file')
    exit(1)
output = sys.argv[1]

api_token = os.environ.get('OSUAPI_TOKEN')
if not api_token:
    print('Please set the OSUAPI_TOKEN variable')
    exit(1)


@dataclass
class Score:
    map_id: int
    enabled_mods: int
    combo: int
    count_100: int
    count_50: int
    miss_count: int
    pp: float


@dataclass
class Beatmap:
    circle_count: int
    slider_count: int
    spinner_count: int
    max_combo: int
    base_ar: float
    base_od: float
    aim_stars: float
    speed_stars: float


print("Sit back, this'll take a while")

print('Grabbing latest maps...')
sets = requests.get(
    'https://osu.ppy.sh/beatmapsets/search').json()['beatmapsets']
map_ids = [b['id']
           for s in sets for b in s['beatmaps'] if b['mode'] == 'osu']

print('Collecting scores...')
scores = []
for id in map_ids:
    map_scores = requests.get(
        f'https://osu.ppy.sh/api/get_scores?k={api_token}&b={id}').json()
    scores += [Score(
        map_id=id,
        enabled_mods=int(s['enabled_mods']),
        combo=int(s['maxcombo']),
        count_100=int(s['count100']),
        count_50=int(s['count50']),
        miss_count=int(s['countmiss']),
        pp=float(s['pp']),
    ) for s in map_scores[::5]]
    time.sleep(2)
    print('.', end='', flush=True)

print()
print("Fetching maps' stars...")
beatmaps = {}
diff_changing_mods = 338
for sc in scores:
    masked_mods = sc.enabled_mods & diff_changing_mods
    beatmap_with_mods = beatmaps.get((sc.map_id, masked_mods))
    if beatmap_with_mods is None:
        info = requests.get(
            f'https://osu.ppy.sh/api/get_beatmaps?k={api_token}&b={sc.map_id}&mods={masked_mods}').json()[0]
        beatmaps[(sc.map_id, masked_mods)] = Beatmap(
            circle_count=int(info['count_normal']),
            slider_count=int(info['count_slider']),
            spinner_count=int(info['count_spinner']),
            max_combo=int(info['max_combo']),
            base_ar=float(info['diff_approach']),
            base_od=float(info['diff_overall']),
            aim_stars=float(info['diff_aim']),
            speed_stars=float(info['diff_speed']),
        )
        time.sleep(2)
        print('.', end='', flush=True)

print()
print(f'Saving to {output}')
with open(output, 'w') as f:
    f.write('# map id,circles,sliders,spinners,map combo,base ar,base od'
            + ',aim stars,speed stars,mods,combo,100,50,misses,pp\n')
    for sc in scores:
        masked_mods = sc.enabled_mods & diff_changing_mods
        b = beatmaps[(sc.map_id, masked_mods)]
        f.write(
            f'{sc.map_id},{b.circle_count},{b.slider_count},{b.spinner_count},' +
            f'{b.max_combo},{b.base_ar},{b.base_od},{b.aim_stars},{b.speed_stars},' +
            f'{sc.enabled_mods},{sc.combo},{sc.count_100},' +
            f'{sc.count_50},{sc.miss_count},{sc.pp}\n'
        )

print("Done!")
