package skybot

import (
	"regexp"
	"strconv"
	"strings"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"golang.org/x/xerrors"
)

type commandHandler func(*Bot, *discord.Replier, *discord.Message, string) error
type settingHandler func(*Bot, *discord.Replier, db.HierLevel, string, string) error

func showUser(b *Bot, r *discord.Replier, m *discord.Message, argStr string) error {
	args, err := newOsuArgs(b, argStr, m, hasUsersRange(1, 4))
	if err != nil {
		return xerrors.Errorf("parse arguments: %w", err)
	}
	users, err := args.getUsers(b.Osu, b.Settings)
	if err != nil {
		return xerrors.Errorf("get users: %w", err)
	}

	return r.ReplyEmbed(userEmbed(users))
}

func showBeatmap(b *Bot, r *discord.Replier, m *discord.Message, argStr string) error {
	args, err := newOsuArgs(b, argStr, m, hasBeatmapsRange(1, 1), hasMaxMods(1))
	if err != nil {
		return xerrors.Errorf("parse arguments: %w", err)
	}
	beatmaps, err := args.getBeatmaps(b.Osu, b.Settings)
	if err != nil {
		return xerrors.Errorf("get users: %w", err)
	}

	_ = b.Settings.Set(db.LevelChannel, m.ChannelID, "lastMap", strconv.Itoa(beatmaps[0].ID))
	return r.ReplyEmbed(beatmapEmbed(beatmaps, args.ppcalcArgs))
}

func bindOsuID(b *Bot, r *discord.Replier, level db.HierLevel, id string, argStr string) error {
	if len(argStr) == 0 {
		return friendlyerr.Wrapf(xerrors.New("missing argument"), errBindOsuIDMissingName(cmdPrefix))
	}

	if level != db.LevelUser {
		return friendlyerr.Wrapf(xerrors.New("unsupported db level"), errSettingUserOnly)
	}

	u, err := userByName{name: argStr}.get(b.Osu, b.Settings)
	if err != nil {
		return err
	}

	err = b.Settings.Set(db.LevelUser, id, "osuID", strconv.Itoa(u.ID))
	if err != nil {
		return xerrors.Errorf("db save: %w", err)
	}

	return r.Replyf(successBindOsuID(u.Name, u.ID))
}

var aliasArgsRe = regexp.MustCompile(`(?i)^(.+?)\s+for\s+([^,]+)$`)

func addAliases(b *Bot, r *discord.Replier, level db.HierLevel, id string, argStr string) error {
	match := aliasArgsRe.FindStringSubmatch(argStr)
	if len(match) == 0 {
		return friendlyerr.Wrapf(xerrors.New("invalid args"), errAddAliases(cmdPrefix))
	}

	fullName := match[2]
	u, err := userByName{name: fullName}.get(b.Osu, b.Settings)
	if err != nil {
		return err
	}

	shortNames := strings.Split(match[1], ",")
	cleanedShortNames := make([]string, 0, len(shortNames))
	for _, n := range shortNames {
		n = strings.ToLower(strings.TrimSpace(n))
		if n == "" {
			continue
		}

		err = b.Settings.Set(level, id, "alias:"+n, strconv.Itoa(u.ID))
		if err != nil {
			return xerrors.Errorf("db save: %w", err)
		}
		cleanedShortNames = append(cleanedShortNames, n)
	}

	return r.Replyf(successAddAliases(u.Name, u.ID, cleanedShortNames...))
}

func settingsHandler(handlers map[string]settingHandler) commandHandler {
	return func(b *Bot, r *discord.Replier, m *discord.Message, argStr string) error {
		args, err := newSettingArgs(b, argStr, m)
		if err != nil {
			return xerrors.Errorf("parse/validate command: %w", err)
		}

		h, ok := handlers[strings.ToLower(args.command)]
		if !ok {
			return friendlyerr.Wrapf(xerrors.New("unknown setting"), errUnknownSetting(args.command))
		}

		return h(b, r, args.level, args.id, args.argStr)
	}
}
