package skybot

import (
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/osu"
	"golang.org/x/xerrors"
)

var specialTokens = []struct {
	re      *regexp.Regexp
	command string
	apply   func(a *osuArgs, match []string)
}{
	{ // Discord mention
		regexp.MustCompile(`<@!?(\d+)>`), "user", nil,
	},
	{ // Link to osu! user
		regexp.MustCompile(`<?\bhttps?://osu\.ppy\.sh/u(?:sers)?/(\d+)[^,\s]*>?`),
		"user", func(a *osuArgs, match []string) {
			a.userGetters = append(a.userGetters, userByID{id: match[1]})
		},
	},
	{ // Link to osu! beatmap
		regexp.MustCompile(`<?\bhttps?://osu\.ppy\.sh/(?:b(?:eatmaps)?/(\d+)|beatmapsets/\d+#\w+/(\d+))[^,\s]*>?`),
		"map", func(a *osuArgs, match []string) {
			a.beatmapGetters = append(a.beatmapGetters, beatmapByID{id: match[1] + match[2]})
		},
	},
	{ // Link to osu! mapset
		regexp.MustCompile(`<?\bhttps?://osu\.ppy\.sh/(?:s|beatmapsets)/(\d+)[^,\s]*>?`),
		"map", func(a *osuArgs, match []string) {
			a.beatmapGetters = append(a.beatmapGetters, beatmapBySetID{id: match[1]})
		},
	},
	{ // Mods
		regexp.MustCompile(`\+((?:[a-zA-Z]{2},?\??)+)`),
		"map", func(a *osuArgs, match []string) {
			a.modStr += match[1]
		},
	},
	{ // Nx100 (alternative x is a Russian letter)
		regexp.MustCompile(`(?i)(\d+)[xх]100`),
		"map", func(a *osuArgs, match []string) {
			a.initPPCalcArgs()
			a.count100, _ = strconv.Atoi(match[1])
		},
	},
	{ // Nx50
		regexp.MustCompile(`(?i)(\d+)[xх]50`),
		"map", func(a *osuArgs, match []string) {
			a.initPPCalcArgs()
			a.count50, _ = strconv.Atoi(match[1])
		},
	},
	{ // Nx (combo)
		regexp.MustCompile(`(?i)(\d+)[xх]`),
		"map", func(a *osuArgs, match []string) {
			a.initPPCalcArgs()
			a.combo, _ = strconv.Atoi(match[1])
		},
	},
	{ // Nm (number of misses; alternative м is Russian)
		regexp.MustCompile(`(?i)(\d+)(?:miss|m|м)`),
		"map", func(a *osuArgs, match []string) {
			a.initPPCalcArgs()
			a.missCount, _ = strconv.Atoi(match[1])
		},
	},
}

var commandNameRe = regexp.MustCompile(`^[\pL-]+`)

//https://github.com/ppy/osu-web/blob/master/app/Libraries/UsernameValidation.php#L78
var usernameRe = regexp.MustCompile(`[a-zA-Z0-9\[\]_ ]{3,15}`)

func splitCommandName(msg string) (name, args string) {
	for _, t := range specialTokens {
		pos := t.re.FindStringIndex(msg)
		if len(pos) > 0 && pos[0] == 0 {
			return t.command, msg
		}
	}
	name = commandNameRe.FindString(msg)
	return name, strings.TrimSpace(strings.TrimPrefix(msg, name))
}

type osuArgs struct {
	authorID    string
	useAuthorID bool
	userGetters []userGetter

	useLastMap     bool
	beatmapGetters []beatmapGetter

	modStr string
	mods   []osu.Mods

	*ppcalcArgs
}

func newOsuArgs(b *Bot, argStr string, m *discord.Message, asserts ...argAssertion) (*osuArgs, error) {
	var a osuArgs
	a.authorID = m.AuthorID

	for _, ment := range m.Mentions {
		a.userGetters = append(a.userGetters, userByMention{id: ment.ID})
	}
	argStr = discord.ReplaceMentions(argStr, m.Mentions, ",")

	for _, t := range specialTokens {
		if t.apply == nil {
			continue
		}
		for _, match := range t.re.FindAllStringSubmatch(argStr, -1) {
			t.apply(&a, match)
		}
		argStr = t.re.ReplaceAllLiteralString(argStr, ",")
	}

	for _, tok := range strings.Split(argStr, ",") {
		tok = strings.TrimSpace(tok)
		if tok == "" {
			continue
		}

		g, err := dealiasedGetter(tok, m.ServerID, m.ChannelID, m.AuthorID, b.Settings)
		if err != nil {
			return nil, err
		}
		a.userGetters = append(a.userGetters, g)
	}

	for _, ass := range asserts {
		if err := ass(&a); err != nil {
			return nil, err
		}
	}

	if a.useAuthorID {
		a.userGetters = append([]userGetter{author{id: a.authorID}}, a.userGetters...)
	}
	if a.useLastMap {
		a.beatmapGetters = append([]beatmapGetter{lastMap{chanID: m.ChannelID}}, a.beatmapGetters...)
	}
	if a.mods == nil {
		a.mods = []osu.Mods{osu.NoMod}
	}

	return &a, nil
}

type argAssertion func(*osuArgs) error

func hasUsersRange(min, max int) argAssertion {
	return func(a *osuArgs) error {
		n := len(a.userGetters)
		if n < min-1 || n > max {
			return friendlyerr.Wrapf(xerrors.Errorf("need %d-%d users, have %d", min, max, n),
				errArgsNeedUsersRange(min, max, n))
		}

		if n == min-1 {
			a.useAuthorID = true
		}
		return nil
	}
}

func hasBeatmapsRange(min, max int) argAssertion {
	return func(a *osuArgs) error {
		n := len(a.beatmapGetters)
		if n < min-1 || n > max {
			return friendlyerr.Wrapf(xerrors.Errorf("need %d-%d beatmaps, have %d", min, max, n),
				errArgsNeedBeatmapsRange(min, max, n))
		}

		if n == min-1 {
			a.useLastMap = true
		}
		return nil
	}
}

func hasMaxMods(max int) argAssertion {
	return func(a *osuArgs) error {
		m, err := expandMods(a.modStr, max)
		if err != nil {
			return err
		}
		a.mods = m
		return nil
	}
}

func (a *osuArgs) getUsers(osuapi OsuAPI, settings DB) ([]*osu.User, error) {
	users := make([]*osu.User, len(a.userGetters))
	for i, g := range a.userGetters {
		u, err := g.get(osuapi, settings)
		if err != nil {
			return nil, err
		}
		users[i] = u
	}
	return users, nil
}

func (a *osuArgs) getBeatmaps(osuapi OsuAPI, settings DB) ([]*osu.Beatmap, error) {
	beatmaps := make([]*osu.Beatmap, len(a.beatmapGetters))
	for i, g := range a.beatmapGetters {
		for _, m := range a.mods {
			b, err := g.get(osuapi, settings, m)
			if err != nil {
				return nil, err
			}
			beatmaps[i] = b
		}
	}
	return beatmaps, nil
}

type userGetter interface {
	get(OsuAPI, DB) (*osu.User, error)
}

type author struct{ id string }
type userByMention struct{ id string }
type userByID struct{ id string }
type userByName struct{ name string }

func (a author) get(osuapi OsuAPI, settings DB) (*osu.User, error) {
	osuID := getOsuID(a.id, settings)
	if osuID == "" {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("unknown author"),
			errUnknownOsuIDAuthor(cmdPrefix))
	}
	u, err := osuapi.GetUserByID(osuID)
	if err != nil {
		return nil, xerrors.Errorf("get author: %w", err)
	}
	if u == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("restricted author"),
			errDeletedOsuUserAuthor(cmdPrefix))
	}
	return u, nil
}

func (g userByMention) get(osuapi OsuAPI, settings DB) (*osu.User, error) {
	osuID := getOsuID(g.id, settings)
	if osuID == "" {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("unresolved mention @%s", g.id),
			errUnknownOsuIDOther(g.id, cmdPrefix))
	}
	u, err := osuapi.GetUserByID(osuID)
	if err != nil {
		return nil, xerrors.Errorf("get user %s: %w", osuID, err)
	}
	if u == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("no osu! user %s", osuID),
			errDeletedOsuUserOther(osuID, cmdPrefix))
	}
	return u, nil
}

func (g userByID) get(osuapi OsuAPI, settings DB) (*osu.User, error) {
	u, err := osuapi.GetUserByID(g.id)
	if err != nil {
		return nil, xerrors.Errorf("get user %s: %w", g.id, err)
	}
	if u == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("no osu! user %s", g.id),
			errNoUserWithID(g.id))
	}
	return u, nil
}

func (g userByName) get(osuapi OsuAPI, settings DB) (*osu.User, error) {
	u, err := osuapi.GetUserByName(g.name)
	if err != nil {
		return nil, xerrors.Errorf("get user %s: %w", g.name, err)
	}
	if u == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("no osu! user %s", g.name),
			errNoUserWithName(g.name))
	}
	return u, nil
}

func getOsuID(discordID string, settings DB) string {
	return settings.Get(db.LevelUser, discordID, "osuID")
}

func dealiasedGetter(name, serverID, channelID, discordID string, settings DB) (userGetter, error) {
	resolved := settings.GetAny(serverID, channelID, discordID, "alias:"+strings.ToLower(name))
	switch resolved {
	case "":
		if !usernameRe.MatchString(name) {
			return nil, friendlyerr.Wrapf(xerrors.Errorf("unknown token: %s", name),
				errUnknownToken(name))
		}
		return userByName{name: name}, nil
	default:
		return userByID{id: resolved}, nil
	}
}

type beatmapGetter interface {
	get(OsuAPI, DB, osu.Mods) (*osu.Beatmap, error)
}

type lastMap struct{ chanID string }
type beatmapByID struct{ id string }
type beatmapBySetID struct{ id string }

func (g lastMap) get(osuapi OsuAPI, settings DB, mods osu.Mods) (*osu.Beatmap, error) {
	id := settings.Get(db.LevelChannel, g.chanID, "lastMap")
	if id == "" {
		return nil, friendlyerr.Wrapf(
			xerrors.Errorf("no last used map in %s", g.chanID), errNoLastUsedBeatmap)
	}
	return beatmapByID{id: id}.get(osuapi, settings, mods)
}

func (g beatmapByID) get(osuapi OsuAPI, _ DB, mods osu.Mods) (*osu.Beatmap, error) {
	b, err := osuapi.GetBeatmap(g.id, mods)
	if err != nil {
		return nil, xerrors.Errorf("get beatmap %s: %w", g.id, err)
	}
	if b == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("no beatmap %s", g.id), errNoBeatmap)
	}
	return b, nil
}

func (g beatmapBySetID) get(osuapi OsuAPI, _ DB, mods osu.Mods) (*osu.Beatmap, error) {
	b, err := osuapi.GetBeatmapSet(g.id, mods)
	if err != nil {
		return nil, xerrors.Errorf("get beatmapset %s: %w", g.id, err)
	}
	if b == nil {
		return nil, friendlyerr.Wrapf(xerrors.Errorf("no beatmapset %s", g.id), errNoBeatmap)
	}
	sort.Sort(osu.ByDifficulty(b))
	return b[len(b)-1], nil
}

func (a *osuArgs) initPPCalcArgs() {
	if a.ppcalcArgs == nil {
		a.ppcalcArgs = &ppcalcArgs{}
	}
}

type ppcalcArgs struct {
	count100  int
	count50   int
	missCount int
	combo     int
}
