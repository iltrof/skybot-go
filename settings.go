package skybot

import (
	"strings"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"golang.org/x/xerrors"
)

type settingArgs struct {
	command string
	level   db.HierLevel
	id      string
	argStr  string
}

func newSettingArgs(b *Bot, argStr string, m *discord.Message) (*settingArgs, error) {
	toks := strings.Fields(argStr)
	if len(toks) == 0 {
		return nil, friendlyerr.Wrapf(xerrors.New("missing arguments"), errSettingArgsMissingSubcommand)
	}

	s := settingArgs{level: db.LevelUser}
	if l, ok := settingLevels[strings.ToLower(toks[0])]; ok {
		s.level = l
		toks = toks[1:]
		if len(toks) == 0 {
			return nil, friendlyerr.Wrapf(xerrors.New("missing subcommand"), errSettingArgsMissingSubcommand)
		}
	}

	switch s.level {
	case db.LevelUser:
		s.id = m.AuthorID
	case db.LevelChannel:
		s.id = m.ChannelID
		hasPerm, err := b.Discord.AuthorCanManageChannel(m)
		if err != nil {
			return nil, friendlyerr.Wrapf(
				xerrors.Errorf("check permissions: %w", err), errManageChannelCheckFailed)
		}
		if !hasPerm {
			return nil, friendlyerr.Wrapf(xerrors.New("no permission"), errCannotManageChannel)
		}
	case db.LevelServer:
		s.id = m.ServerID
		hasPerm, err := b.Discord.AuthorCanManageServer(m)
		if err != nil {
			return nil, friendlyerr.Wrapf(
				xerrors.Errorf("check permissions: %w", err), errManageServerCheckFailed)
		}
		if !hasPerm {
			return nil, friendlyerr.Wrapf(xerrors.New("no permission"), errCannotManageServer)
		}
	}

	s.command = toks[0]
	s.argStr = strings.Join(toks[1:], " ")
	return &s, nil
}

var settingLevels = map[string]db.HierLevel{
	"u": db.LevelUser, "user": db.LevelUser,
	"c": db.LevelChannel, "chan": db.LevelChannel, "channel": db.LevelChannel,
	"s": db.LevelServer, "serv": db.LevelServer, "server": db.LevelServer,
}
