package db

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB_PutGet(t *testing.T) {
	levels := []HierLevel{LevelUser, LevelChannel, LevelServer}
	for _, level := range levels {
		t.Run(string(level), func(t *testing.T) {
			f, err := ioutil.TempFile("", "")
			require.NoError(t, err)
			f.Close()
			defer os.Remove(f.Name())

			db, err := Open(f.Name())
			require.NoError(t, err)
			defer db.Close()

			v := db.Get(level, "1", "foo")
			assert.Equal(t, "", v)

			assert.NoError(t, db.Set(level, "1", "foo", "bar1"))
			assert.NoError(t, db.Set(level, "2", "foo", "bar2"))
			v = db.Get(level, "1", "foo")
			assert.Equal(t, "bar1", v)
			v = db.Get(level, "2", "foo")
			assert.Equal(t, "bar2", v)
		})
	}
}

func TestDB_GetAny(t *testing.T) {
	f, err := ioutil.TempFile("", "")
	require.NoError(t, err)
	f.Close()
	defer os.Remove(f.Name())

	db, err := Open(f.Name())
	require.NoError(t, err)
	defer db.Close()

	assert.NoError(t, db.Set(LevelServer, "1", "foo", "serv"))
	assert.NoError(t, db.Set(LevelChannel, "2", "foo", "chan"))
	assert.NoError(t, db.Set(LevelUser, "3", "foo", "user"))

	v := db.GetAny("0", "0", "0", "foo")
	assert.Equal(t, "", v)

	v = db.GetAny("1", "0", "0", "foo")
	assert.Equal(t, "serv", v)

	v = db.GetAny("1", "2", "0", "foo")
	assert.Equal(t, "chan", v)

	v = db.GetAny("1", "2", "3", "foo")
	assert.Equal(t, "user", v)
}
