package db

import (
	"github.com/etcd-io/bbolt"
	"golang.org/x/xerrors"
)

// DB represents a persistent database for strings with three
// hierarchy levels: server-wide, channel-wide and single-user.
type DB struct {
	boltDB *bbolt.DB
}

// Open opens a database in the given file.
func Open(filename string) (DB, error) {
	db, err := bbolt.Open(filename, 0666, nil)
	if err != nil {
		return DB{}, xerrors.Errorf("db open: %w", err)
	}

	err = db.Update(func(tx *bbolt.Tx) error {
		return createBuckets(tx, []HierLevel{LevelUser, LevelChannel, LevelServer})
	})
	if err != nil {
		return DB{}, xerrors.Errorf("db init: %w", err)
	}
	return DB{boltDB: db}, nil
}

// Close closes the database.
func (db DB) Close() {
	db.boltDB.Close()
}

// Set stores a value on the given hierarchy level.
// ID refers to the ID of the object on that level, e.g. for
// server-wide values it is the ID of the server.
func (db DB) Set(level HierLevel, id, key, value string) error {
	// if this was a general package, this would be problematic,
	// but in context of skybot this is fine
	key = id + key

	return db.boltDB.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(level))
		return b.Put([]byte(key), []byte(value))
	})
}

// GetAny returns a value for either the given server, channel or the user.
// More specific hierarchy levels take precedence, i.e. a single-user value
// is preferred over a channel-wide value, and a channel-wide value over a
// server-wide value.
func (db DB) GetAny(servID, chanID, userID, key string) string {
	precedence := []HierLevel{LevelUser, LevelChannel, LevelServer}
	ids := []string{userID, chanID, servID}
	for i, level := range precedence {
		if v := db.Get(level, ids[i], key); v != "" {
			return v
		}
	}
	return ""
}

// Get returns a stored value from the given hierarchy level.
// ID refers to the ID of the object on that level, e.g. for
// server-wide values it is the ID of the server.
func (db DB) Get(level HierLevel, id, key string) string {
	key = id + key
	var value string
	_ = db.boltDB.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(level))
		value = string(b.Get([]byte(key)))
		return nil
	})
	return value
}

// HierLevel represents a hierarchy level.
type HierLevel string

// Hierarchy levels.
const (
	LevelUser    HierLevel = "user"
	LevelChannel HierLevel = "channel"
	LevelServer  HierLevel = "server"
)

func createBuckets(tx *bbolt.Tx, levels []HierLevel) error {
	var err error
	for _, n := range levels {
		_, err = tx.CreateBucketIfNotExists([]byte(n))
		if err != nil {
			return xerrors.Errorf("create bucket %s: %w", n, err)
		}
	}
	return nil
}
