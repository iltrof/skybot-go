module github.com/iltrof/skybot

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/etcd-io/bbolt v1.3.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/stretchr/testify v1.3.0
	go.etcd.io/bbolt v1.3.2 // indirect
	golang.org/x/sys v0.0.0-20190412213103-97732733099d // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
)

go 1.12
