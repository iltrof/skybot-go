package cache

import (
	"time"

	"github.com/patrickmn/go-cache"
)

// Cache is a cache.
type Cache struct {
	c *cache.Cache
}

// New creates a new cache.
func New() Cache {
	return Cache{c: cache.New(24*time.Hour, 10*time.Minute)}
}

// Set sets a value in the cache.
func (c Cache) Set(k string, v interface{}, expires time.Duration) {
	c.c.Set(k, v, expires)
}

// Get gets a value from the cache.
func (c Cache) Get(k string) (v interface{}, ok bool) {
	return c.c.Get(k)
}
