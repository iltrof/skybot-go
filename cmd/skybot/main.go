package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/iltrof/skybot"
	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/osu"
)

func main() {
	osuapi := osu.NewClient(mustGetenv("OSUAPI_TOKEN"), &http.Client{})
	disc, err := discord.New(mustGetenv("DISCORD_TOKEN"))
	check(err)
	db, err := db.Open("settings.db")
	check(err)

	bot := skybot.Bot{
		Discord:  disc,
		Osu:      osuapi,
		Settings: db,
	}
	bot.Run()
	defer bot.Stop()

	fmt.Println("Bot is now running, press Ctrl+C to quit")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func mustGetenv(name string) string {
	v := os.Getenv(name)
	if v == "" {
		fmt.Printf("Please provide the environment variable: %s\n", name)
		os.Exit(1)
	}
	return v
}
