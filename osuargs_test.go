package skybot

import (
	"testing"

	"github.com/iltrof/skybot/db"
	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/mocks"
	"github.com/iltrof/skybot/osu"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/xerrors"
)

func Test_splitCommandName(t *testing.T) {
	tests := []struct {
		name     string
		msg      string
		wantName string
		wantArgs string
	}{
		{"empty", "", "", ""},
		{"no args", "foo", "foo", ""},
		{"with args", "foo arg1 arg2", "foo", "arg1 arg2"},
		{"only letters and dash", "foo-абв123", "foo-абв", "123"},

		{"mention", "<@123>", "user", "<@123>"},
		{"mention 2", "<@!123>", "user", "<@!123>"},

		{"osu! user link", "http://osu.ppy.sh/u/123", "user", "http://osu.ppy.sh/u/123"},
		{"osu! user link2", "<https://osu.ppy.sh/users/123/osu>", "user", "<https://osu.ppy.sh/users/123/osu>"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			name, args := splitCommandName(tt.msg)
			assert.Equal(t, tt.wantName, name)
			assert.Equal(t, tt.wantArgs, args)
		})
	}
}

func Test_newOsuArgs(t *testing.T) {
	tests := []struct {
		name string
		args string
		msg  *discord.Message
		want *osuArgs
	}{
		{"empty", "", &discord.Message{}, &osuArgs{}},
		{"one username", "nathan on osu", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "nathan on osu"},
			}},
		},
		{"two usernames", "nathan on osu, rafis", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "nathan on osu"},
				userByName{name: "rafis"},
			}},
		},

		{"mentions", "<@1234> <@567>",
			&discord.Message{Mentions: []discord.Mention{{ID: "1234"}, {ID: "567"}}},
			&osuArgs{userGetters: []userGetter{
				userByMention{id: "1234"}, userByMention{id: "567"},
			}},
		},
		{"mention within text", "abc<@123>def",
			&discord.Message{Mentions: []discord.Mention{{ID: "123"}}},
			&osuArgs{userGetters: []userGetter{
				userByMention{id: "123"},
				userByName{name: "abc"},
				userByName{name: "def"},
			}},
		},
		{"escaped mention", `\<@123>`, &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByName{name: `\<@123>`},
			}},
		},

		{"osu! user link", "http://osu.ppy.sh/u/123", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByID{id: "123"},
			}},
		},
		{"osu! user link 2", "<https://osu.ppy.sh/users/123/osu>", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByID{id: "123"},
			}},
		},
		{"osu! user link within text", "abc https://osu.ppy.sh/users/123 def", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByID{id: "123"},
				userByName{name: "abc"},
				userByName{name: "def"},
			}},
		},
		{"fake osu! user link", "foohttp://osu.ppy.sh/u/123", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "foohttp://osu.ppy.sh/u/123"},
			}},
		},

		{"spaces", "  nathan on osu  , \n rafis \t ", &discord.Message{},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "nathan on osu"},
				userByName{name: "rafis"},
			}},
		},

		{"osu! beatmap link", "http://osu.ppy.sh/b/123", &discord.Message{},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapByID{id: "123"},
			}},
		},
		{"osu! beatmap link 2", "<https://osu.ppy.sh/beatmaps/123>", &discord.Message{},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapByID{id: "123"},
			}},
		},
		{"osu! beatmap link 3", "https://osu.ppy.sh/beatmapsets/1#osu/123", &discord.Message{},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapByID{id: "123"},
			}},
		},
		{"osu! beatmapset link", "https://osu.ppy.sh/s/123", &discord.Message{},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapBySetID{id: "123"},
			}},
		},
		{"osu! beatmapset link 2", "https://osu.ppy.sh/beatmapsets/123", &discord.Message{},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapBySetID{id: "123"},
			}},
		},

		{"mods1", "+hddt", &discord.Message{},
			&osuArgs{modStr: "hddt"}},
		{"mods2", "+hddt +pf", &discord.Message{},
			&osuArgs{modStr: "hddtpf"}},
		{"mods3", "+hd?nc?", &discord.Message{},
			&osuArgs{modStr: "hd?nc?"}},

		{"n100_1", "17x100", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count100: 17}}},
		{"n100_2", "17X100", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count100: 17}}},
		{"n100_3", "17х100", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count100: 17}}},
		{"n50_1", "17x50", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count50: 17}}},
		{"n50_2", "17X50", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count50: 17}}},
		{"n50_3", "17х50", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{count50: 17}}},
		{"nmiss_1", "17m", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{missCount: 17}}},
		{"nmiss_2", "17м", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{missCount: 17}}},
		{"nmiss_3", "17miss", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{missCount: 17}}},
		{"nmiss_4", "17MiSs", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{missCount: 17}}},
		{"combo_1", "17x", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{combo: 17}}},
		{"combo_2", "17X", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{combo: 17}}},
		{"combo_3", "17х", &discord.Message{},
			&osuArgs{ppcalcArgs: &ppcalcArgs{combo: 17}}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			settings := mocks.DB{}
			settings.On("GetAny", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("")

			tt.want.mods = []osu.Mods{osu.NoMod}
			args, err := newOsuArgs(&Bot{Settings: &settings}, tt.args, tt.msg)
			assert.NoError(t, err)
			assert.Equal(t, tt.want, args)
		})
	}
}

func Test_newOsuArgs_errors(t *testing.T) {
	settings := mocks.DB{}
	settings.On("GetAny", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("")

	a, err := newOsuArgs(&Bot{Settings: &settings}, ")!@(*#", &discord.Message{})
	assert.Nil(t, a)
	assert.Equal(t, errUnknownToken(")!@(*#"), friendlyerr.Description(err))
}

func Test_newOsuArgs_aliases(t *testing.T) {
	settings := mocks.DB{}
	settings.On("GetAny", "1", "2", "3", "alias:shige").Return("42").Once()
	settings.On("GetAny", "1", "2", "3", "alias:www").Return("43").Once()
	settings.On("GetAny", "1", "2", "3", "alias:rafis").Return("").Once()

	args, err := newOsuArgs(&Bot{Settings: &settings}, "shige, WWW, Rafis",
		&discord.Message{ServerID: "1", ChannelID: "2", AuthorID: "3"})
	assert.NoError(t, err)
	assert.Equal(t, &osuArgs{authorID: "3", userGetters: []userGetter{
		userByID{id: "42"}, userByID{id: "43"}, userByName{name: "Rafis"},
	}, mods: []osu.Mods{osu.NoMod}}, args)
	settings.AssertExpectations(t)
}

func Test_hasUsersRange(t *testing.T) {
	type args struct {
		argStr string
		min    int
		max    int
	}
	tests := []struct {
		name         string
		args         args
		want         *osuArgs
		wantFriendly string
	}{
		{"exact, pass", args{"iltrof", 1, 1},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "iltrof"},
			}}, ""},
		{"exact, fail", args{"iltrof", 0, 0},
			nil, errArgsNeedUsersRange(0, 0, 1)},
		{"range, pass", args{"iltrof, peppy", 1, 2},
			&osuArgs{userGetters: []userGetter{
				userByName{name: "iltrof"},
				userByName{name: "peppy"},
			}}, ""},
		{"range, fail", args{"iltrof, peppy", 0, 1},
			nil, errArgsNeedUsersRange(0, 1, 2)},
		{"use author, pass", args{"", 1, 1},
			&osuArgs{useAuthorID: true, userGetters: []userGetter{author{}}}, ""},
		{"use author, pass2", args{"", 1, 4},
			&osuArgs{useAuthorID: true, userGetters: []userGetter{author{}}}, ""},
		{"use author, pass3", args{"iltrof", 2, 4},
			&osuArgs{useAuthorID: true,
				userGetters: []userGetter{
					author{},
					userByName{name: "iltrof"},
				}}, ""},
		{"use author, fail", args{"", 2, 2},
			nil, errArgsNeedUsersRange(2, 2, 0)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			settings := mocks.DB{}
			settings.On("GetAny", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("")

			if tt.want != nil {
				tt.want.mods = []osu.Mods{osu.NoMod}
			}
			args, err := newOsuArgs(&Bot{Settings: &settings}, tt.args.argStr, &discord.Message{},
				hasUsersRange(tt.args.min, tt.args.max))
			assert.Equal(t, tt.want, args)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
		})
	}
}

func Test_hasBeatmapsRange(t *testing.T) {
	type args struct {
		argStr string
		min    int
		max    int
	}
	tests := []struct {
		name         string
		args         args
		want         *osuArgs
		wantFriendly string
	}{
		{"exact, pass", args{"https://osu.ppy.sh/b/75", 1, 1},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapByID{id: "75"},
			}}, ""},
		{"exact, fail", args{"https://osu.ppy.sh/b/75", 0, 0},
			nil, errArgsNeedBeatmapsRange(0, 0, 1)},
		{"range, pass", args{"https://osu.ppy.sh/b/75, https://osu.ppy.sh/s/1", 1, 2},
			&osuArgs{beatmapGetters: []beatmapGetter{
				beatmapByID{id: "75"},
				beatmapBySetID{id: "1"},
			}}, ""},
		{"range, fail", args{"https://osu.ppy.sh/b/75, https://osu.ppy.sh/s/1", 0, 1},
			nil, errArgsNeedBeatmapsRange(0, 1, 2)},
		{"use last map, pass", args{"", 1, 1},
			&osuArgs{useLastMap: true, beatmapGetters: []beatmapGetter{lastMap{}}}, ""},
		{"use last map, pass2", args{"https://osu.ppy.sh/b/75", 2, 4},
			&osuArgs{useLastMap: true,
				beatmapGetters: []beatmapGetter{
					lastMap{},
					beatmapByID{id: "75"},
				}}, ""},
		{"use last map, fail", args{"", 2, 2},
			nil, errArgsNeedBeatmapsRange(2, 2, 0)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.want != nil {
				tt.want.mods = []osu.Mods{osu.NoMod}
			}
			args, err := newOsuArgs(&Bot{}, tt.args.argStr, &discord.Message{},
				hasBeatmapsRange(tt.args.min, tt.args.max))
			assert.Equal(t, tt.want, args)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
		})
	}
}

func Test_hasMaxMods(t *testing.T) {
	a, err := newOsuArgs(&Bot{}, "+xx", &discord.Message{}, hasMaxMods(1))
	assert.Nil(t, a)
	assert.Error(t, err)

	a, err = newOsuArgs(&Bot{}, "+hd?dt", &discord.Message{}, hasMaxMods(1))
	assert.Nil(t, a)
	assert.Error(t, err)

	a, err = newOsuArgs(&Bot{}, "+hddt", &discord.Message{}, hasMaxMods(1))
	assert.Equal(t, a.mods, []osu.Mods{osu.Hidden | osu.DoubleTime})
	assert.NoError(t, err)
}

func Test_userGetters(t *testing.T) {
	sampleError := friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")

	type osuIDLookup struct {
		enabled   bool
		discordID string
		osuID     string
	}
	tests := []struct {
		name   string
		getter userGetter

		osuIDLookup  osuIDLookup
		osuGetter    string
		osuGetterArg string
		user         *osu.User
		err          error

		wantUser     *osu.User
		wantFriendly string
	}{
		{"unknown author", author{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: ""},
			"", "", nil, nil, nil, errUnknownOsuIDAuthor(cmdPrefix)},
		{"failed author", author{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", nil, sampleError,
			nil, friendlyerr.Description(sampleError)},
		{"non-existing author", author{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", nil, nil,
			nil, errDeletedOsuUserAuthor(cmdPrefix)},
		{"normal author", author{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", sampleUser, nil,
			sampleUser, ""},

		{"unresolved mention", userByMention{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: ""},
			"", "", nil, nil, nil, errUnknownOsuIDOther("42", cmdPrefix)},
		{"failed mention", userByMention{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", nil, sampleError,
			nil, friendlyerr.Description(sampleError)},
		{"non-existing mention", userByMention{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", nil, nil,
			nil, errDeletedOsuUserOther("77", cmdPrefix)},
		{"normal mention", userByMention{id: "42"},
			osuIDLookup{enabled: true, discordID: "42", osuID: "77"},
			"GetUserByID", "77", sampleUser, nil,
			sampleUser, ""},

		{"failed by ID", userByID{id: "77"}, osuIDLookup{},
			"GetUserByID", "77", nil, sampleError,
			nil, friendlyerr.Description(sampleError)},
		{"non-existing by ID", userByID{id: "77"}, osuIDLookup{},
			"GetUserByID", "77", nil, nil,
			nil, errNoUserWithID("77")},
		{"normal by ID", userByID{id: "77"}, osuIDLookup{},
			"GetUserByID", "77", sampleUser, nil,
			sampleUser, ""},

		{"failed by name", userByName{name: "foo"}, osuIDLookup{},
			"GetUserByName", "foo", nil, sampleError,
			nil, friendlyerr.Description(sampleError)},
		{"non-existing by name", userByName{name: "foo"}, osuIDLookup{},
			"GetUserByName", "foo", nil, nil,
			nil, errNoUserWithName("foo")},
		{"normal by name", userByName{name: "foo"}, osuIDLookup{},
			"GetUserByName", "foo", sampleUser, nil,
			sampleUser, ""},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			settings := mocks.DB{}
			osuAPI := mocks.OsuAPI{}

			if tt.osuIDLookup.enabled {
				settings.On("Get", db.LevelUser, tt.osuIDLookup.discordID, "osuID").
					Return(tt.osuIDLookup.osuID).Once()
			}
			if tt.osuGetter != "" {
				osuAPI.On(tt.osuGetter, tt.osuGetterArg).Return(tt.user, tt.err).Once()
			}

			u, err := tt.getter.get(&osuAPI, &settings)
			assert.Equal(t, tt.wantUser, u)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
			settings.AssertExpectations(t)
			osuAPI.AssertExpectations(t)
		})
	}
}

func Test_beatmapByID(t *testing.T) {
	sampleError := friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")

	tests := []struct {
		name         string
		mods         osu.Mods
		beatmap      *osu.Beatmap
		err          error
		wantBeatmap  *osu.Beatmap
		wantFriendly string
	}{
		{"no beatmap", osu.NoMod, nil, nil, nil, errNoBeatmap},
		{"osuapi down", osu.NoMod, nil, sampleError, nil, "Bad thing happened."},
		{"normal", osu.NoMod, &sampleBeatmap, nil, &sampleBeatmap, ""},
		{"modded", osu.Hidden, &sampleBeatmap, nil, &sampleBeatmap, ""},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osuAPI := mocks.OsuAPI{}
			osuAPI.On("GetBeatmap", "75", tt.mods).Return(tt.beatmap, tt.err).Once()

			b, err := beatmapByID{id: "75"}.get(&osuAPI, nil, tt.mods)
			assert.Equal(t, tt.wantBeatmap, b)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
			osuAPI.AssertExpectations(t)
		})
	}
}

func Test_beatmapBySetID(t *testing.T) {
	sampleError := friendlyerr.Wrapf(xerrors.New("bad thing"), "Bad thing happened.")
	harderBeatmap := sampleBeatmap
	harderBeatmap.TotalStars++

	tests := []struct {
		name         string
		mods         osu.Mods
		beatmaps     []*osu.Beatmap
		err          error
		wantBeatmap  *osu.Beatmap
		wantFriendly string
	}{
		{"no beatmapset", osu.NoMod, nil, nil, nil, errNoBeatmap},
		{"osuapi down", osu.NoMod, nil, sampleError, nil, "Bad thing happened."},
		{"normal", osu.NoMod, []*osu.Beatmap{&sampleBeatmap, &harderBeatmap, &sampleBeatmap},
			nil, &harderBeatmap, ""},
		{"modded", osu.Hidden, []*osu.Beatmap{&sampleBeatmap}, nil, &sampleBeatmap, ""},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osuAPI := mocks.OsuAPI{}
			osuAPI.On("GetBeatmapSet", "75", tt.mods).Return(tt.beatmaps, tt.err).Once()

			b, err := beatmapBySetID{id: "75"}.get(&osuAPI, nil, tt.mods)
			assert.Equal(t, tt.wantBeatmap, b)
			assert.Equal(t, tt.wantFriendly, friendlyerr.Description(err))
			osuAPI.AssertExpectations(t)
		})
	}
}

func Test_lastMap(t *testing.T) {
	settings := mocks.DB{}
	settings.On("Get", db.LevelChannel, "123", "lastMap").Return("").Once()

	b, err := lastMap{chanID: "123"}.get(nil, &settings, osu.NoMod)
	assert.Nil(t, b)
	assert.Equal(t, errNoLastUsedBeatmap, friendlyerr.Description(err))

	osuAPI := mocks.OsuAPI{}
	osuAPI.On("GetBeatmap", "75", osu.NoMod).Return(&sampleBeatmap, (error)(nil)).Once()
	settings.On("Get", db.LevelChannel, "123", "lastMap").Return("75").Once()

	b, err = lastMap{chanID: "123"}.get(&osuAPI, &settings, osu.NoMod)
	assert.Equal(t, &sampleBeatmap, b)
	assert.NoError(t, err)

	osuAPI.AssertExpectations(t)
	settings.AssertExpectations(t)
}
