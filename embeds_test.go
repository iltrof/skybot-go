package skybot

import (
	"fmt"
	"testing"
	"time"

	"github.com/iltrof/skybot/discord"
	"github.com/iltrof/skybot/osu"
	"github.com/stretchr/testify/assert"
)

func Test_userEmbed(t *testing.T) {
	want := &discord.Embed{
		Color:        skybotColor,
		ThumbnailURL: sampleUser.AvatarURL,
		Fields: []discord.EmbedField{
			{
				Title: sampleUserEmbedTitle,
				Text:  sampleUserEmbedText,
			},
		},
	}
	assert.Equal(t, want, userEmbed([]*osu.User{sampleUser}))
}

func Test_beatmapEmbed(t *testing.T) {
	want := &discord.Embed{
		Color:        skybotColor,
		ThumbnailURL: "https://assets.ppy.sh/beatmaps/1/covers/list@2x.jpg",
		Fields: []discord.EmbedField{
			{
				Title: sampleBeatmapEmbedTitle,
				Text:  sampleBeatmapEmbedText,
			},
		},
	}
	assert.Equal(t, want, beatmapEmbed([]*osu.Beatmap{&sampleBeatmap}, nil))
}

func Test_beatmapEmbed_ppArgs(t *testing.T) {
	want := &discord.Embed{
		Color:        skybotColor,
		ThumbnailURL: "https://assets.ppy.sh/beatmaps/1/covers/list@2x.jpg",
		Fields: []discord.EmbedField{
			{
				Title: sampleBeatmapEmbedTitle,
				Text:  sampleBeatmapEmbedTextWithPP,
			},
		},
	}
	assert.Equal(t, want, beatmapEmbed([]*osu.Beatmap{&sampleBeatmap}, samplePPCalcArgs))
}

func Test_joinFieldItems(t *testing.T) {
	items := [][]string{
		{"foo", "bar", "baz"},
		{"abc"},
		{},
		{"1", "2"},
	}
	want := "foo | bar | baz\nabc\n\n1 | 2"
	assert.Equal(t, want, joinFieldItems(items, " | "))
}

func Test_format(t *testing.T) {
	type args struct {
		f string
		a []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"simple string", args{"foo", []interface{}{}}, "foo"},
		{"escaped string", args{"**%s**", []interface{}{"_foo bar_"}}, `**\_foo bar\_**`},
		{"numbers", args{"%d %f", []interface{}{1234, 12342.123}}, "1,234 12,342.12"},
		{"trailing percent", args{"foo%", []interface{}{}}, "foo%"},
		{"double percent", args{"foo%%bar", []interface{}{}}, "foo%bar"},
		{"unknown delegated to fmt", args{"%v", []interface{}{42}}, "42"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, format(tt.args.f, tt.args.a...))
		})
	}
}

func Test_f2f(t *testing.T) {
	tests := []struct {
		f    float64
		want string
	}{
		{0, "0"},
		{7, "7"},
		{123, "123"},
		{1234, "1,234"},
		{123456789, "123,456,789"},
		{7.1, "7.1"},
		{7.123, "7.12"},
		{7.129, "7.13"},
		{7.001, "7"},
		{12345.678, "12,345.68"},
		{-12345.678, "-12,345.68"},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.f), func(t *testing.T) {
			assert.Equal(t, tt.want, f2f(tt.f))
		})
	}
}

var sampleUser = &osu.User{
	ID:             39828,
	Name:           "WubWoofWolf",
	CountryCode:    "PL",
	AvatarURL:      "http://a.ppy.sh/39828",
	PP:             12578.1,
	Rank:           45,
	CountryRank:    3,
	Accuracy:       99.17985534667969,
	RankedPlays:    267599,
	RankedPlayTime: 20078383 * time.Second,
	Level:          108.236,
	CountSS:        2994,
	CountSSH:       7560,
	CountS:         781,
	CountSH:        9700,
	CountA:         2597,
}

const sampleUserEmbedTitle = ":flag_pl:\u2002WubWoofWolf"

var sampleUserEmbedText = joinFieldItems([][]string{
	{"**#45** (#3)", "**12,578.1\u00a0pp**"},
	{"99.18%", "267,599\u00a0plays", "lvl\u00a0108"},
	{
		discord.Link("Profile", sampleUser.ProfileURL(), ""),
		discord.Link("osu!chan", "https://syrin.me/osuchan/u/39828", ""),
		discord.Link("osu!daily", "https://osudaily.net/profile.php?u=WubWoofWolf", ""),
	},
}, " \u200b **·** \u200b ")

var sampleBeatmap = osu.Beatmap{
	ID:    75,
	SetID: 1,

	Artist:   "Kenji Ninuma",
	Title:    "DISCO PRINCE",
	Diff:     "Normal",
	Mapper:   "peppy",
	MapperID: 2,
	Mode:     0,
	Status:   1,

	CS: 4,
	OD: 6,
	AR: 6,
	HP: 6,

	BPM:          119.999,
	Length:       109 * time.Second,
	CircleCount:  160,
	SliderCount:  0,
	SpinnerCount: 0,
	MaxCombo:     314,

	TotalStars: 2.4069502353668213,
	AimStars:   1.1959426403045654,
	SpeedStars: 1.2059860229492188,

	Downloadable: true,
}

const sampleBeatmapEmbedTitle = "Kenji Ninuma — DISCO PRINCE [Normal]"

var sampleBeatmapEmbedText = joinFieldItems([][]string{
	{"CS4", "HP6", "OD6", "AR**6**"},
	{"**120**\u00a0BPM", "1:49", "314x", "★**2.41**", "14.26\u00a0pp"},
	{
		discord.Link("Map", "https://osu.ppy.sh/beatmaps/75", ""),
		discord.Link("⇓⇓⇓", "https://osu.ppy.sh/beatmapsets/1/download", "Download"),
		discord.Link("Preview", "https://bloodcat.com/osu/preview.html#75", "Bloodcat preview"),
		discord.Link("peppy", "https://osu.ppy.sh/users/2", "Creator"),
	},
}, " \u200b **·** \u200b ")

var samplePPCalcArgs = &ppcalcArgs{
	count100: 5, count50: 3, missCount: 2, combo: 42,
}

var sampleBeatmapEmbedTextWithPP = sampleBeatmapEmbedText +
	"\n95.1% 2m 42x — **2.77**\u00a0pp"

func Test_formatPPCalcArgs(t *testing.T) {
	tests := []struct {
		objectCount int
		mapMaxCombo int
		ppArgs      ppcalcArgs
		want        string
	}{
		{42, 100, ppcalcArgs{}, "SS"},
		{42, 100, ppcalcArgs{combo: 100}, "SS"},
		{42, 100, ppcalcArgs{count100: 1, count50: 1}, "96.43% FC"},
		{42, 100, ppcalcArgs{combo: 70, missCount: 1}, "97.62% 1m 70x"},
	}
	for _, tt := range tests {
		t.Run(tt.want, func(t *testing.T) {
			got := formatPPCalcArgs(tt.objectCount, tt.mapMaxCombo, tt.ppArgs)
			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_ppcalcArgs_normalized(t *testing.T) {
	maxCombo := 200
	tests := []struct {
		name string
		args *ppcalcArgs
		want ppcalcArgs
	}{
		{"empty", &ppcalcArgs{}, ppcalcArgs{combo: 200}},
		{"only combo", &ppcalcArgs{combo: 150},
			ppcalcArgs{combo: 150, count100: 1}},
		{"only combo 2", &ppcalcArgs{combo: 50},
			ppcalcArgs{combo: 50, count100: 3}},
		{"only acc", &ppcalcArgs{count100: 5, count50: 3},
			ppcalcArgs{combo: 200, count100: 5, count50: 3}},
		{"only miss", &ppcalcArgs{missCount: 2},
			ppcalcArgs{combo: 198, missCount: 2}},
		{"miss and combo", &ppcalcArgs{combo: 150, missCount: 3},
			ppcalcArgs{combo: 150, missCount: 3}},
		{"miss and combo 2", &ppcalcArgs{combo: 50, missCount: 1},
			ppcalcArgs{combo: 50, missCount: 1, count100: 2}},
		{"combo and acc", &ppcalcArgs{combo: 50, count100: 1},
			ppcalcArgs{combo: 50, count100: 3}},
		{"everything", &ppcalcArgs{combo: 50, count100: 1, count50: 1, missCount: 1},
			ppcalcArgs{combo: 50, count100: 1, count50: 1, missCount: 1}},

		{"combo edge case", &ppcalcArgs{combo: 199},
			ppcalcArgs{combo: 199, count100: 1}},
		{"combo edge case 2", &ppcalcArgs{combo: 66},
			ppcalcArgs{combo: 66, count100: 2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.args.normalized(maxCombo)
			assert.Equal(t, tt.want, got)
		})
	}
}
