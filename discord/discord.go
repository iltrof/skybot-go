//go:generate mockery -name=dgSession -case=underscore -inpkg

package discord

import (
	"fmt"
	"strings"
	"sync"

	"github.com/bwmarrin/discordgo"
	"golang.org/x/xerrors"
)

// Client is a client that can communicate with discord.
type Client struct {
	session dgSession
	selfID  string
	state   *discordgo.State
}

// New creates a new client.
func New(token string) (*Client, error) {
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		return nil, xerrors.Errorf("create session: %w", err)
	}

	err = dg.Open()
	if err != nil {
		return nil, xerrors.Errorf("open connection: %w", err)
	}

	return &Client{session: dg, selfID: dg.State.User.ID, state: dg.State}, nil
}

// AddHandler adds a handler for newly received messages.
func (c *Client) AddHandler(h func(*Replier, *Message)) {
	c.session.AddHandler(func(_ *discordgo.Session, dgm *discordgo.MessageCreate) {
		if dgm.Author.ID == c.selfID {
			return
		}

		m := newMessage(dgm.Message)
		h(c.newReplier(m), m)
	})
}

// Close terminates the client.
func (c *Client) Close() error {
	return c.session.Close()
}

func (c *Client) newReplier(m *Message) *Replier {
	var once sync.Once
	return &Replier{
		Replyf: func(format string, a ...interface{}) error {
			var err error
			once.Do(func() {
				_, err = c.session.ChannelMessageSend(m.ChannelID, fmt.Sprintf(format, a...))
			})
			if err != nil {
				return xerrors.Errorf("failed to send: %w", err)
			}
			return nil
		},
		ReplyEmbed: func(e *Embed) error {
			var err error
			once.Do(func() {
				_, err = c.session.ChannelMessageSendEmbed(m.ChannelID, e.dgEmbed())
			})
			if err != nil {
				return xerrors.Errorf("failed to send: %w", err)
			}
			return nil
		},
	}
}

// Replier wraps functions to reply to a message.
type Replier struct {
	Replyf     func(format string, a ...interface{}) error
	ReplyEmbed func(*Embed) error
}

// Message represents a Discord message.
type Message struct {
	AuthorID    string
	ChannelID   string
	ServerID    string
	Text        string
	Attachments []string
	Mentions    []Mention

	authorCanManageChannel *bool
	authorCanManageServer  *bool
}

// Mention contains information about
// a user mentioned in a message.
type Mention struct {
	ID   string
	Name string
}

func newMessage(msg *discordgo.Message) *Message {
	var attachments []string
	for _, a := range msg.Attachments {
		attachments = append(attachments, a.URL)
	}
	var mentions []Mention
	for _, m := range msg.Mentions {
		mentions = append(mentions, Mention{ID: m.ID, Name: m.Username})
	}
	return &Message{
		AuthorID:    msg.Author.ID,
		ChannelID:   msg.ChannelID,
		ServerID:    msg.GuildID,
		Text:        msg.Content,
		Attachments: attachments,
		Mentions:    mentions,
	}
}

// AuthorCanManageChannel returns true if the author of
// the message has permissions to manage the channel the
// message was sent in.
func (c *Client) AuthorCanManageChannel(m *Message) (bool, error) {
	if m.authorCanManageChannel == nil {
		err := c.getPermissions(m)
		if err != nil {
			return false, err
		}
	}
	return *m.authorCanManageChannel, nil
}

// AuthorCanManageServer returns true if the author of
// the message has permissions to manage the server the
// message was sent in.
func (c *Client) AuthorCanManageServer(m *Message) (bool, error) {
	if m.authorCanManageServer == nil {
		err := c.getPermissions(m)
		if err != nil {
			return false, err
		}
	}
	return *m.authorCanManageServer, nil
}

func (c *Client) getPermissions(m *Message) error {
	perms, err := c.state.UserChannelPermissions(m.AuthorID, m.ChannelID)
	if err != nil {
		return xerrors.Errorf("get permissions: %w", err)
	}

	canManageChannel := perms&discordgo.PermissionManageChannels != 0
	canManageServer := perms&discordgo.PermissionManageServer != 0
	m.authorCanManageChannel = &canManageChannel
	m.authorCanManageServer = &canManageServer
	return nil
}

// SetAuthorManageChannelPerm makes Client.AuthorCanManageChannel
// return the value given to this function. ONLY FOR TESTS.
func (m *Message) SetAuthorManageChannelPerm(allow bool) {
	m.authorCanManageChannel = &allow
}

// SetAuthorManageServerPerm makes Client.AuthorCanManageServer
// return the value given to this function. ONLY FOR TESTS.
func (m *Message) SetAuthorManageServerPerm(allow bool) {
	m.authorCanManageServer = &allow
}

// ReplaceMentions replaces given mentions in a string.
func ReplaceMentions(s string, ments []Mention, repl string) string {
	for _, m := range ments {
		s = strings.ReplaceAll(s, "<@"+m.ID+">", repl)
		s = strings.ReplaceAll(s, "<@!"+m.ID+">", repl)
	}
	return s
}

// Embed represents a discord embed.
type Embed struct {
	Color        int
	Fields       []EmbedField
	ThumbnailURL string
}

// EmbedField represents a field inside an embed.
type EmbedField struct {
	Title string
	Text  string
}

func (e *Embed) dgEmbed() *discordgo.MessageEmbed {
	if e == nil {
		return nil
	}

	var fields []*discordgo.MessageEmbedField
	for _, f := range e.Fields {
		fields = append(fields, &discordgo.MessageEmbedField{
			Name:  f.Title,
			Value: f.Text,
		})
	}
	return &discordgo.MessageEmbed{
		Color: e.Color,
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: e.ThumbnailURL,
		},
		Fields: fields,
	}
}

type dgSession interface {
	Close() error
	AddHandler(interface{}) func()

	ChannelMessageSend(string, string) (*discordgo.Message, error)
	ChannelMessageSendEmbed(string, *discordgo.MessageEmbed) (*discordgo.Message, error)
}
