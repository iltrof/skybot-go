package disctest

import (
	"fmt"
	"sync"

	"github.com/iltrof/skybot/discord"
)

// ReplyRecorder is a helper type that records
// calls to a discord.Replier.
type ReplyRecorder struct {
	// Text contains the formatted string produced by the latest Replyf call.
	Text string

	// Embed contains the argument of the latest ReplyEmbed call.
	Embed *discord.Embed
}

// Replier returns a new discord.Replier whose
// function calls will be recorded.
func (rr *ReplyRecorder) Replier() *discord.Replier {
	var once sync.Once
	return &discord.Replier{
		Replyf: func(format string, a ...interface{}) error {
			once.Do(func() {
				rr.Text = fmt.Sprintf(format, a...)
			})
			return nil
		},
		ReplyEmbed: func(e *discord.Embed) error {
			once.Do(func() {
				rr.Embed = e
			})
			return nil
		},
	}
}
