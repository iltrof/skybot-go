package disctest

import (
	"testing"

	"github.com/iltrof/skybot/discord"
	"github.com/stretchr/testify/assert"
)

func TestReplyRecorder_Text(t *testing.T) {
	rr := ReplyRecorder{}
	r := rr.Replier()

	assert.NoError(t, r.Replyf("Sample text: %s", "foo"))
	assert.Equal(t, "Sample text: foo", rr.Text)
	assert.Nil(t, rr.Embed)
}

func TestReplyRecorder_Embed(t *testing.T) {
	rr := ReplyRecorder{}
	r := rr.Replier()

	assert.NoError(t, r.ReplyEmbed(sampleEmbed))
	assert.Equal(t, "", rr.Text)
	assert.Equal(t, sampleEmbed, rr.Embed)
}

func TestReplyRecorder_Once(t *testing.T) {
	rr := ReplyRecorder{}
	r := rr.Replier()

	assert.NoError(t, r.Replyf("foo"))
	assert.NoError(t, r.ReplyEmbed(sampleEmbed))
	assert.NoError(t, r.Replyf("bar"))
	assert.Equal(t, "foo", rr.Text)
	assert.Nil(t, rr.Embed)
}

var sampleEmbed = &discord.Embed{
	ThumbnailURL: "http://example.com",
	Fields: []discord.EmbedField{
		{Title: "foo", Text: "bar"},
	},
}
