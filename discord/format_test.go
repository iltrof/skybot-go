package discord

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEscape(t *testing.T) {
	ascii := ` !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_` +
		"`" + `abcdefghijklmnopqrstuvwxyz{|}~`
	want := ` \!\"\#\$\%\&\'\(\)\*\+\,\-\.\/0123456789` +
		`\:\;\<\=\>\?\@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\\]\^\_` +
		"\\`" + `abcdefghijklmnopqrstuvwxyz\{\|\}\~`
	assert.Equal(t, want, Escape(ascii))
}

func TestLink(t *testing.T) {
	want := `[foobar](http://example.com?foo=with+spaces "qu\"ux")`
	assert.Equal(t, want, Link(`foobar`, `http://example.com?foo=with spaces`, `qu"ux`))
}

func TestMentionString(t *testing.T) {
	assert.Equal(t, "<@123>", MentionString("123"))
}

func TestFlag(t *testing.T) {
	tests := []struct {
		name string
		code string
		want string
	}{
		{"normal", "de", ":flag_de:"},
		{"uppercase", "DE", ":flag_de:"},
		{"invalid", "XX", ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, Flag(tt.code))
		})
	}
}
