package discord

import (
	"fmt"
	"net/url"
	"strings"
)

// Escape escapes all ASCII non-digit and non-letter
// characters from the string. This prevents the text
// from being parsed as markdown.
func Escape(s string) string {
	b := strings.Builder{}
	b.Grow(len(s))
	for _, r := range s {
		if (r > ' ' && r < '0') || (r > '9' && r < 'A') || (r > 'Z' && r < 'a') || (r > 'z' && r < 127) {
			b.WriteRune('\\')
		}
		b.WriteRune(r)
	}
	return b.String()
}

// Link returns a formatted link.
func Link(text, rawurl, hover string) string {
	u, err := url.Parse(rawurl)
	if err == nil {
		u.RawQuery = u.Query().Encode()
		rawurl = u.String()
	}
	return fmt.Sprintf(`[%s](%s %q)`, text, rawurl, hover)
}

// MentionString returns a string mentioning a user.
func MentionString(userID string) string {
	return fmt.Sprintf("<@%s>", userID)
}

// Flag returns a flag emoji for the given country code,
// or empty string if the country code is unsupported/invalid.
func Flag(countryCode string) string {
	countryCode = strings.ToLower(countryCode)
	if _, ok := flags[countryCode]; !ok {
		return ""
	}
	return fmt.Sprintf(":flag_%s:", countryCode)
}

var flags = map[string]bool{
	"ac": true, "ad": true, "ae": true, "af": true, "ag": true, "ai": true,
	"al": true, "am": true, "ao": true, "aq": true, "ar": true, "as": true,
	"at": true, "au": true, "aw": true, "ax": true, "az": true, "ba": true,
	"bb": true, "bd": true, "be": true, "bf": true, "bg": true, "bh": true,
	"bi": true, "bj": true, "bl": true, "bm": true, "bn": true, "bo": true,
	"bq": true, "br": true, "bs": true, "bt": true, "bv": true, "bw": true,
	"by": true, "bz": true, "ca": true, "cc": true, "cd": true, "cf": true,
	"cg": true, "ch": true, "ci": true, "ck": true, "cl": true, "cm": true,
	"cn": true, "co": true, "cp": true, "cr": true, "cu": true, "cv": true,
	"cw": true, "cx": true, "cy": true, "cz": true, "de": true, "dg": true,
	"dj": true, "dk": true, "dm": true, "do": true, "dz": true, "ea": true,
	"ec": true, "ee": true, "eg": true, "eh": true, "er": true, "es": true,
	"et": true, "eu": true, "fi": true, "fj": true, "fk": true, "fm": true,
	"fo": true, "fr": true, "ga": true, "gb": true, "gd": true, "ge": true,
	"gf": true, "gg": true, "gh": true, "gi": true, "gl": true, "gm": true,
	"gn": true, "gp": true, "gq": true, "gr": true, "gs": true, "gt": true,
	"gu": true, "gw": true, "gy": true, "hk": true, "hm": true, "hn": true,
	"hr": true, "ht": true, "hu": true, "ic": true, "id": true, "ie": true,
	"il": true, "im": true, "in": true, "io": true, "iq": true, "ir": true,
	"is": true, "it": true, "je": true, "jm": true, "jo": true, "jp": true,
	"ke": true, "kg": true, "kh": true, "ki": true, "km": true, "kn": true,
	"kp": true, "kr": true, "kw": true, "ky": true, "kz": true, "la": true,
	"lb": true, "lc": true, "li": true, "lk": true, "lr": true, "ls": true,
	"lt": true, "lu": true, "lv": true, "ly": true, "ma": true, "mc": true,
	"md": true, "me": true, "mf": true, "mg": true, "mh": true, "mk": true,
	"ml": true, "mm": true, "mn": true, "mo": true, "mp": true, "mq": true,
	"mr": true, "ms": true, "mt": true, "mu": true, "mv": true, "mw": true,
	"mx": true, "my": true, "mz": true, "na": true, "nc": true, "ne": true,
	"nf": true, "ng": true, "ni": true, "nl": true, "no": true, "np": true,
	"nr": true, "nu": true, "nz": true, "om": true, "pa": true, "pe": true,
	"pf": true, "pg": true, "ph": true, "pk": true, "pl": true, "pm": true,
	"pn": true, "pr": true, "ps": true, "pt": true, "pw": true, "py": true,
	"qa": true, "re": true, "ro": true, "rs": true, "ru": true, "rw": true,
	"sa": true, "sb": true, "sc": true, "sd": true, "se": true, "sg": true,
	"sh": true, "si": true, "sj": true, "sk": true, "sl": true, "sm": true,
	"sn": true, "so": true, "sr": true, "ss": true, "st": true, "sv": true,
	"sx": true, "sy": true, "sz": true, "ta": true, "tc": true, "td": true,
	"tf": true, "tg": true, "th": true, "tj": true, "tk": true, "tl": true,
	"tm": true, "tn": true, "to": true, "tr": true, "tt": true, "tv": true,
	"tw": true, "tz": true, "ua": true, "ug": true, "um": true, "us": true,
	"uy": true, "uz": true, "va": true, "vc": true, "ve": true, "vg": true,
	"vi": true, "vn": true, "vu": true, "wf": true, "ws": true, "xk": true,
	"ye": true, "yt": true, "za": true, "zm": true, "zw": true,
}
