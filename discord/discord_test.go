package discord

import (
	"testing"

	"github.com/bwmarrin/discordgo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/xerrors"
)

func Test_newMessage(t *testing.T) {
	msg := &discordgo.Message{
		ChannelID: "123",
		GuildID:   "42",
		Content:   "foo",
		Author: &discordgo.User{
			ID: "321",
		},
		Attachments: []*discordgo.MessageAttachment{
			{
				URL: "http://example.com",
			},
		},
		Mentions: []*discordgo.User{
			{
				ID:       "777",
				Username: "bar",
			},
		},
	}
	want := &Message{
		AuthorID:    "321",
		ChannelID:   "123",
		ServerID:    "42",
		Text:        "foo",
		Attachments: []string{"http://example.com"},
		Mentions:    []Mention{{ID: "777", Name: "bar"}},
	}
	assert.Equal(t, want, newMessage(msg))
}

func TestEmbed_dgEmbed(t *testing.T) {
	embed := Embed{
		Color: 0xAABBCC,
		Fields: []EmbedField{
			{Title: "xxx", Text: "yyy"},
		},
		ThumbnailURL: "http://example.com",
	}
	want := &discordgo.MessageEmbed{
		Color: 0xAABBCC,
		Fields: []*discordgo.MessageEmbedField{
			{Name: "xxx", Value: "yyy"},
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: "http://example.com",
		},
	}
	assert.Equal(t, want, embed.dgEmbed())
}

func TestEmbed_dgEmbed_nil(t *testing.T) {
	var embed *Embed
	assert.Equal(t, (*discordgo.MessageEmbed)(nil), embed.dgEmbed())
}

func TestClient_AddHandler(t *testing.T) {
	sess := mockDgSession{}
	var hand func(*discordgo.Session, *discordgo.MessageCreate)
	sess.On("AddHandler", mock.AnythingOfType("func(*discordgo.Session, *discordgo.MessageCreate)")).Once().
		Run(func(args mock.Arguments) {
			hand = args.Get(0).(func(*discordgo.Session, *discordgo.MessageCreate))
		}).Return((func())(nil))

	c := Client{session: &sess, selfID: "777"}

	count := 0
	c.AddHandler(func(r *Replier, m *Message) {
		count++
		assert.Equal(t, sampleMessage, m)
	})

	hand(nil, &discordgo.MessageCreate{
		Message: &discordgo.Message{
			Author: &discordgo.User{ID: c.selfID},
		},
	})
	assert.Equal(t, 0, count, "if author's ID is client's own ID, handler should not be called")

	hand(nil, &discordgo.MessageCreate{Message: sampleDGMessage})
	assert.Equal(t, 1, count)
	sess.AssertExpectations(t)
}

func TestClient_newReplier(t *testing.T) {
	sess := mockDgSession{}
	sampleErr := xerrors.New("bad thing")
	sess.On("ChannelMessageSend", sampleMessage.ChannelID, "42").
		Once().Return((*discordgo.Message)(nil), sampleErr)
	sess.On("ChannelMessageSendEmbed", sampleMessage.ChannelID, mock.AnythingOfType("*discordgo.MessageEmbed")).
		Once().Run(func(args mock.Arguments) {
		assert.Equal(t, sampleDGEmbed.Fields, args.Get(1).(*discordgo.MessageEmbed).Fields)
	}).Return((*discordgo.Message)(nil), sampleErr)

	c := Client{session: &sess}

	r := c.newReplier(sampleMessage)
	assert.True(t, xerrors.Is(r.Replyf("%d", 42), sampleErr))

	// sess.AssertExpectations will test that the replier
	// doesn't consider further calls to its functions
	assert.NoError(t, r.ReplyEmbed(sampleEmbed))
	assert.NoError(t, r.Replyf("%d", 42))

	r = c.newReplier(sampleMessage)
	assert.True(t, xerrors.Is(r.ReplyEmbed(sampleEmbed), sampleErr))

	// sess.AssertExpectations will test that the replier
	// doesn't consider further calls to its functions
	assert.NoError(t, r.Replyf("%d", 42))
	assert.NoError(t, r.ReplyEmbed(sampleEmbed))

	sess.AssertExpectations(t)
}

func TestClient_Close(t *testing.T) {
	sess := mockDgSession{}
	sess.On("Close").Once().Return((error)(nil))
	c := Client{session: &sess}
	assert.NoError(t, c.Close())
	sess.AssertExpectations(t)
}

func TestReplaceMentions(t *testing.T) {
	assert.Equal(t, "_ <@456> _",
		ReplaceMentions("<@123> <@456> <@!789>", []Mention{{ID: "123"}, {ID: "789"}}, "_"))
}

func TestMessage_AllowAuthorManageChannel(t *testing.T) {
	c, m := Client{}, Message{}

	m.SetAuthorManageChannelPerm(true)
	can, err := c.AuthorCanManageChannel(&m)
	assert.NoError(t, err)
	assert.True(t, can)

	m.SetAuthorManageChannelPerm(false)
	can, err = c.AuthorCanManageChannel(&m)
	assert.NoError(t, err)
	assert.False(t, can)
}

func TestMessage_AllowAuthorManageServer(t *testing.T) {
	c, m := Client{}, Message{}

	m.SetAuthorManageServerPerm(true)
	can, err := c.AuthorCanManageServer(&m)
	assert.NoError(t, err)
	assert.True(t, can)

	m.SetAuthorManageServerPerm(false)
	can, err = c.AuthorCanManageServer(&m)
	assert.NoError(t, err)
	assert.False(t, can)
}

var sampleDGMessage = &discordgo.Message{
	Author:    &discordgo.User{ID: "42"},
	ChannelID: "123",
	Content:   "foo",
}

var sampleMessage = &Message{
	AuthorID:  "42",
	ChannelID: "123",
	Text:      "foo",
}

var sampleDGEmbed = &discordgo.MessageEmbed{
	Fields: []*discordgo.MessageEmbedField{
		{Name: "foo", Value: "bar"},
	},
}

var sampleEmbed = &Embed{
	Fields: []EmbedField{
		{Title: "foo", Text: "bar"},
	},
}
