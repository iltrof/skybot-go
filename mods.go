package skybot

import (
	"strings"

	"github.com/iltrof/skybot/friendlyerr"
	"github.com/iltrof/skybot/osu"
	"golang.org/x/xerrors"
)

func expandMods(s string, maxCombinations int) ([]osu.Mods, error) {
	if 1<<uint(strings.Count(s, "?")) > maxCombinations {
		return nil, friendlyerr.Wrapf(xerrors.New("too many mods"), errTooManyMods(maxCombinations))
	}

	combinations := expandModsQuestionMarks(s)

	result := map[osu.Mods]bool{}
	resultExtended := map[osu.Mods]bool{}
	for _, c := range combinations {
		m, err := osu.ModsFromString(c)
		if err != nil {
			return nil, friendlyerr.Wrapf(err, errInvalidMods)
		}
		result[m] = true
		resultExtended[m] = true

		if m&osu.DoubleTime != 0 {
			resultExtended[m&^osu.Nightcore] = true
			resultExtended[m|osu.Nightcore] = true
		}
		if m&osu.SuddenDeath != 0 {
			resultExtended[m&^osu.Perfect] = true
			resultExtended[m|osu.Perfect] = true
		}
		if m&(osu.SuddenDeath|osu.DoubleTime) == osu.SuddenDeath|osu.DoubleTime {
			resultExtended[m&^(osu.Perfect|osu.Nightcore)] = true
			resultExtended[m|(osu.Perfect|osu.Nightcore)] = true
		}
	}

	if len(resultExtended) <= maxCombinations {
		result = resultExtended
	}
	mods := make([]osu.Mods, 0, len(result))
	for m := range result {
		mods = append(mods, m)
	}
	return mods, nil
}

func expandModsQuestionMarks(s string) []string {
	combinations := []string{s}
	for i := 0; i < strings.Count(s, "?"); i++ {
		for j, c := range combinations {
			q := strings.IndexRune(c, '?')
			combinations[j] = c[:q] + c[q+1:]
			combinations = append(combinations, c[:q-2]+c[q+1:])
		}
	}
	return combinations
}
